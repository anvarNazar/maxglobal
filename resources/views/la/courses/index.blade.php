@extends("la.layouts.app")

@section("contentheader_title", "Courses")
@section("contentheader_description", "Courses listing")
@section("section", "Courses")
@section("sub_section", "Listing")
@section("htmlheader_title", "Courses Listing")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal" id="addCourseModal" data-target="#AddModal"> <i class="icon-plus"></i> Add Courses</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-hover mg-table ">
		<thead>
			<tr class="success">
				<th>Course name</th>
				<th>Course description</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		if(count($courses) > 0) { 
			foreach($courses as $result) {
		?>
			<tr role="row" class="odd">
				<td><a href="javascript:void(0);"><?php echo $result->course_name; ?></a></td>
				<td><?php echo $result->course_description; ?></td>
				<td>
					<a href="#" class="btn btn-warning btn-xs mx-course-edit" data-desc="<?php echo $result->course_description; ?>" data-title="<?php echo $result->course_name; ?>" data-id="<?php echo $result->id; ?>"><i class="fa fa-edit"></i></a>
					<?php
					$output  = Form::open(['route' => [config('laraadmin.adminRoute') . '.courses.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
					
					echo $output;
					?>
				</td>
			</tr>	
		<?php 
			}
		} else { ?>
			<td>No entry found!!</td>
		<?php 
		}
		?>			
		</tbody>
		</table>
	</div>
</div>

@endsection

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Courses</h4>
			</div>
			{!! Form::open(['action' => 'LA\CousesController@store', 'id' => 'course-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" value="" id="course_id" name="course_id">
					<div class="form-group">
						<label for="name">Course name* :</label>
						<input class="form-control" placeholder="Enter course" data-rule-maxlength="25" required id="course_name" name="course_name" type="text" value="">
					</div>					
					<div class="form-group">
						<label for="address">Course description :</label>
						<textarea class="form-control" placeholder="Enter course description" data-rule-maxlength="1000" id="course_desc" cols="30" rows="3" name="course_desc"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@push('scripts')
<script>
$(function () {
	$("#course-add-form").validate({
	});
	$('body').off('click', '.mx-course-edit');
	$('body').on('click', '.mx-course-edit', function() {
		
		var course_id = $(this).attr('data-id');
		var title = $(this).attr('data-title');
		var desc = $(this).attr('data-desc');
		$("#course_id").val(course_id);
		$("#course_name").val(title);
		$("#course_desc").val(desc);
		$('#AddModal').modal('show');
	});
	$("#AddModal").on('hide.bs.modal', function(){
		$('#AddModal input,#AddModal textarea').val('');
	});
});
</script>
@endpush
