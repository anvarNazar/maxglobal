<div id="tab_1" class="tab-pane active" >
        
                   
<div class="content-head">
                                        
                                        <h4>Personal Information</h4>
                                        <div class="id" id="nurse_id">Nurse ID : <b>#12675</b></div>
                        </div>
                <div class="pane" >
                        <div class="row">
                                <div class="col s6 ipfield">
									<div class="input-field">
										<i class="material-icons prefix">account_circle</i>
										<input name="title" id="title" type="text">
										<label for="title">Title</label>
									</div>
                                </div>
                                <div class=" col s6 ipfield">
									<div class="input-field">
										<input name="first_name" required id="first_name" type="text">
										<label for="first_name">First Name</label>
						   
										<span class="helper-text" data-error="wrong"
										data-success="right">As per Passport</span>
									</div>
                                </div>								
                        </div>
                        <div class="row">
                                <div class=" col s6 ipfield">
                                                <div class="input-field">
													<i class="material-icons prefix">account_circle</i>
													<input id="middle_name" name="middle_name" type="text" class="validate">
													<label for="middle_name">Middle Name</label>
													<span class="helper-text" data-error="wrong"
													data-success="right">As per Passport</span>
                                                </div>


                                        </div>
                                        <div class=" col s6 ipfield">
                                                        <div class="input-field">
                                                                        <input id="sur_name" name="sur_name" type="text" class="validate">
                                                                        <label for="sur_name">Sur Name</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>

                        </div>
						<div class="row">
							<div class=" col s12 ipfield">
									<div class="input-field">
													<i class="material-icons prefix">mail</i>
													<input name="email" id="email" required type="text">
													<label for="email">Email</label>
									   
									</div>
							</div>
                        </div>
						
                        <div class="row">
                                <div class="input-field col s12">
                                                <i class="material-icons prefix">calendar_today</i>

                                        <input id="dob" required  name="dob" type="text" class="datepicker">
                                        <label for="dob">Date of Birth</label>
                                        <span class="helper-text" data-error="wrong"
                                        data-success="right">As per Passport</span>
                                </div>

                        </div>
						
						<div class="row">
							<div class="input-field col s12">
											<i class="material-icons prefix">home</i>
							  <textarea id="home_address" name="home_address" required class="materialize-textarea"></textarea>
							  <label for="home_address">Address</label>
							</div>
						</div>
						
						<div class="row">
							<div class="col s7 ipfield">
								<label>Profile image</label>
								<div class="file-field input-field"></div>
								<div class="up-files clearfix">
									<div class="uploaded-file" id="profile_image" data-type="file"></div>	
								</div>
							</div>
						</div>						

						<div class="row">
                                <div class=" col s12 ipfield">
                                        <div class="input-field">
                                                        <input name="mobile" id="mobile" required type="number">
                                                        <label for="mobile">Mobile number </label>
                                           
                                        </div>
                                </div>
                        </div>
						<div class="row">
                                <div class=" col s12 ipfield">
                                        <div class="input-field">
                                                        <input name="nationality" id="nationality" required type="text">
                                                        <label for="nationality">Nationality </label>
                                           
                                        </div>
                                </div>
                         </div>
						<div class="col s12">
							<p>Gender</p>
								<p>
									<label>
										<input name="gender" type="radio" value="MALE" checked/>
										<span>MALE</span>
									</label>
									<label>
										<input name="gender" type="radio"  value="FEMALE" />
										<span>FEMALE</span>
									</label>
							  </p>
							  <br>
						</div>						 
                </div>
</div>