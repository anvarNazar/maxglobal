<div id="tab_8" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Visa Details </h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                </div>
                                <div class="pane">
                                        <div class="row">
                                                <div class=" col s12 ">
                                                    <p>Have You Applied For Visa ?</p>
													<p>
														<label>
																		<input name="visa_status" type="radio" value="1" />
																		<span>Yes</span>
																	  </label>
																	  <label>
																				<input name="visa_status" type="radio"  value="0" />
																				<span>No</span>
																			  </label>
													</p>
                                                                      <br>
                                                </div>

                                        </div>
											
										<div id="visa-details-wrapper" style="display:none;">
											<div class="row">
													<div class=" col s6 ">
															<div class="input-field">
																	<label>Visa  No:</label>
																	<input name="visa_no" id="visa_no" type="text">												
																	
															</div>
													</div>
													<div class="col s6">
														<div class="input-field">											
															<input name="visa_status_text" id="visa_status_text" type="text">		
															<label for="visa_status_text">Visa Status </label>
														</div>
													</div>
											</div>	
											
											<div class="row">
												<div class="col s4">
													<div class="input-field">
															<input name="visa_appt_date" id="visa_appt_date" type="text">												
															<label for="visa_appt_date">Visa Appointment Date </label>
													</div>
												</div>
												<div class="col s4">
													<div class="input-field">
															<input name="visa_reciev_date" id="visa_reciev_date" type="text">												
															<label for="visa_reciev_date">Visa Received Date </label>
													</div>
												</div>
												<div class="col s4">
													<div class="input-field">
															<input name="visa_exp_date" id="visa_exp_date" type="text">												
															<label for="visa_exp_date">Visa Expiry Date  </label>
													</div>
												</div>											
											</div>
											
											<div class="row">
												<div class="col s6">
													<label>Visa Copy File Upload </label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="visa_file" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="visa_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="visa_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>														
												</div>
												
												<div class="col s6">
													<label>Visa Approval Letter File Upload </label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="visa_appr_file" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="visa_appr_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="visa_appr_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>													
												</div>											
											</div>
										</div>
										
										<div class="row">
											<div class="col s6">
												<div class="input-field">											
													<input name="cbt_attempt_date" id="cbt_attempt_date" type="text">	
													<label for="cbt_attempt_date">Flight Ticket Status </label>
												</div>
											</div>
										</div>	
										
										<div class="row" id="flight-ticket-wrapper" style="display:none;">
											<div class="col s6">
												<label>Flight Ticket File Upload </label>
												<div class="file-field input-field"></div>
												<div class="uploaded-file" id="flight_ticket" data-type="file"></div>
												<div class="file-approve-buttons hide">
													<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="flight_ticket" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
													<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="flight_ticket" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
												</div>													
											</div>											
										</div>										
                                </div>									
                        

</div>

                