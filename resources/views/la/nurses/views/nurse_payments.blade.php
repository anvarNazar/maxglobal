<div id="tab_10" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Section 10 </h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                </div>
                                <div class="pane">
								
                                        <div class="row">
											<div class="col s4">
												<div class="input-field">											
													<input name="max_invoice_status" id="max_invoice_status" type="text">	
													<label for="max_invoice_status">Max Invoice Status To Employer  </label>
												</div>
											</div>
											<div class="col s4 inv-status-wrapper" style="display:none;">
												<div class="input-field">
													<input name="employer_pay" id="employer_pay" type="text">
													<label for="employer_pay">Employer Payment Status </label>
												</div>
											</div>
											<div class="col s4 outs-status-wrapper" style="display:none;">
												<div class="input-field">
														<input name="outstanding_pay" id="outstanding_pay" type="text">												
														<label for="outstanding_pay">Outstanding Payment From Employer   </label>
												</div>
											</div>											
                                        </div>
														
                                        <div class="row inv-status-wrapper" style="display:none;">
											<div class=" col s6 ">
													<div class="input-field">
															<label>Max Invoice To Employer </label>
															<input name="max_inv_empl" id="max_inv_empl" type="text">												
															
													</div>
											</div>
                                        </div>	
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">											
													<input name="sub_agents" id="sub_agents" type="text">
													<label for="sub_agents">Sub Agents </label>
												</div>
											</div>
											<div class="col s6 sub-agent-wrapper" style="display:none;">
												<div class="input-field">
													<label>Max Invoice To Employer </label>
													<input name="inv_sub_agent" id="inv_sub_agent" type="text">	
												</div>
											</div>										
                                        </div>
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<input name="sub_agents" id="sub_agents" type="text">
													<label for="sub_agents">Sub Agent Payment Status </label>
												</div>
											</div>											
											<div class=" col s6 " id="paid-wrapper" style="display:none;">
													<div class="input-field">
															<label>Out Standing Payment </label>
															<input name="outstanding_pay_subagent" id="outstanding_pay_subagent" type="text" maxlength="250">												
															
													</div>
											</div>
                                        </div>								
										
										<div class="row">
											<div class="col s6">
												<label>Additional Documents </label>
												<div class="file-field input-field"></div>
												<div class="uploaded-file" id="addl_docs" data-type="file"></div>
												<div class="file-approve-buttons hide">
													<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="addl_docs" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
													<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="addl_docs" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
												</div>													
											</div>										
										</div>
										
										<div class="row">
											<div class="col s12">
												<div class="input-field">											
													<label>Last Communication with Candidate  </label>
													<input name="last_communication" id="last_communication" type="text" maxlength="250">												
												</div>
											</div>
										</div>										
                                </div>									
                        

</div>

                