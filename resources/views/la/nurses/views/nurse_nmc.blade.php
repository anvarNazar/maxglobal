<div id="tab_6" class="tab-pane " >
        
<div class="content-head">
                                                <h4>NMC Details</h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                </div>
                                <div class="pane">

                                        <div class="row">
                                                <div class=" col s12 ">
                                                    <p>Have You Received NMC Decision Letter ?</p>
													<p>
														<label>
																		<input name="nmc_status" type="radio" value="1" />
																		<span>Yes</span>
																	  </label>
																	  <label>
																				<input name="nmc_status" type="radio"  value="0" />
																				<span>No</span>
																			  </label>
													</p>
                                                                      <br>
                                                </div>

                                        </div>
                                        <div id="nmc-details" style="display: none;">
                                        <div class="row">
											<div class=" col s6 ">
													<div class="input-field">
															<input name="nmc_appl_status" id="nmc_appl_status" type="text">
															<label for="nmc_appl_status">NMC Full Application Status  </label>
													</div>
											</div>
											<div class=" col s6 ">
													<div class="input-field">
															<input name="nmc_subs_date" id="nmc_subs_date" type="text">												
															<label for="nmc_subs_date">NMC Full Application Submission Date </label>
													</div>
											</div>
                                        </div>
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
														<input name="nmc_recieved_date" id="nmc_recieved_date" type="text">												
														<label for="nmc_recieved_date">NMC Decision Letter Received Date  </label>
												</div>
											</div>
                                        </div>
										
                                        <div class="row">
                                                <div class="col s3">
                                                    <label>NMC Decision Letter File Upload </label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="nmc_file" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="nmc_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="nmc_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                        

</div>

                