<div id="tab_3" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>Section 3</h4>
                                                        <div class="id">Nurse ID : <b>#12675</b></div>
                                        </div>
                                <div class="pane">
                               
                                        <div class="row">
                                                <div class=" col s7 ipfield">
                                                        <div class="input-field">
                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                        <input id="passport_no" name="passport_no" type="text" class="validate">
                                                                        <label for="passport_no" >Passport Number</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>

                                        </div>
                                        <br>
                                        <div class="row">
                                                <div class="col s6">
                                                    <label>Upload Passport Front Page</label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="image_front" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="image_front" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="image_front" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>	
                                                </div>
                                                <div class="col s6">
                                                    <label>Upload Passport Back Page</label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="image_back" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="image_back" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="image_back" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>	
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Qualifications File Upload </label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="qualification_file" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="qualification_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="qualification_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>	
                                                </div>
                                                <div class="col s6">
                                                        <label>Birth Certificate File Upload </label>
 														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="birth_certificate" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="birth_certificate" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="birth_certificate" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>	
												</div>
                                        </div>
										<div class="row">
												<div class="col s6 ipfield">
													<div class="input-field">
														<input name="nmc_registration" id="nmc_registration" type="text">
														<label for="nmc_registration">NMC Registration Status</label>														
													</div>
												</div>
												<div class="col s6 ipfield">
													<div class="input-field">
														<input name="ielts_passed_date" id="ielts_passed_date" type="text">
														<label for="first_name">IELTS / OET Passed Date</label>
														<span class="helper-text" data-error="wrong" data-success="right"></span>
													</div>
												</div>								
										</div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>IELTS / OET TRF File Upload  </label>
 														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="ielts_file" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="ielts_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="ielts_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>	
												</div>
												<div class="col s6 ipfield">
													<div class="input-field">
														<input name="ielts_score" id="ielts_score" type="text">
														<label for="first_name">IELTS / OET Score</label>
														<span class="helper-text" data-error="wrong" data-success="right"></span>
													</div>
												</div>												
                                        </div>										

                                </div>
</div>