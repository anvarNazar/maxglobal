<div id="tab_6" class="tab-pane " >
        
<div class="content-head">
                                                <h4>NMC Details</h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">

                                        <div class="row">
                                                <div class=" col s12 ">
                                                    <p>Have You Received NMC Decision Letter ?</p>
													<p>
														<label>
																		<input name="nmc_status" type="radio" value="1" />
																		<span>Yes</span>
																	  </label>
																	  <label>
																				<input name="nmc_status" type="radio"  value="0" />
																				<span>No</span>
																			  </label>
													</p>
                                                                      <br>
                                                </div>

                                        </div>
                                        <div id="nmc-details" style="display: none;">
                                        <div class="row">
											<div class=" col s6 ">
													<div class="input-field">
															<select name="nmc_appl_status" id="nmc_appl_status">
																<option value="not-applicable">Not Applicable</option>
																<option value="submitted">Submitted</option>
																<option value="not-submitted">Not Submitted</option>
															</select>
															<label for="nmc_appl_status">NMC Full Application Status  </label>
													</div>
											</div>
											<div class=" col s6 ">
													<div class="input-field">
															<input name="nmc_subs_date" id="nmc_subs_date" class="datepicker" type="text">												
															<label for="nmc_subs_date">NMC Full Application Submission Date </label>
													</div>
											</div>
                                        </div>
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
														<input name="nmc_recieved_date" id="nmc_recieved_date" class="datepicker" type="text">												
														<label for="nmc_recieved_date">NMC Decision Letter Received Date  </label>
												</div>
											</div>
                                        </div>
										
                                        <div class="row">
                                                <div class="col s3">
                                                        <label>NMC Decision Letter File Upload </label>
                                                        <div class="file-field input-field">
																<div class="file-field input-field">
																				<div class="btn">
																						<span>Browse</span>
																						<input type="file" name="nmc_file">
																				</div>
																		<div class="file-path-wrapper">
																				<input class="file-path validate">

																				<div>
																				</div>
																
																		</div>
																</div>

																<div class="uploaded-file" id="nmc_file" data-type="file"></div>																
                                                        </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
								
                                <div class="controls">
                                                <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="nmc">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                