<div id="tab_8" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Visa Details </h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">
                                        <div class="row">
                                                <div class=" col s12 ">
                                                    <p>Have You Applied For Visa ?</p>
													<p>
														<label>
																		<input name="visa_status" type="radio" value="1" />
																		<span>Yes</span>
																	  </label>
																	  <label>
																				<input name="visa_status" type="radio"  value="0" />
																				<span>No</span>
																			  </label>
													</p>
                                                                      <br>
                                                </div>

                                        </div>
											
										<div id="visa-details-wrapper" style="display:none;">
											<div class="row">
													<div class=" col s6 ">
															<div class="input-field">
																	<label>Visa  No:</label>
																	<input name="visa_no" id="visa_no" type="text" maxlength="17">												
																	
															</div>
													</div>
													<div class="col s6">
														<div class="input-field">											
															<select name="visa_status_text" id="visa_status_text">
																<option value="Submitted-Application-Online">Submitted Application Online</option>
																<option value="Documents-Submitted">Documents Submitted To Home Office</option>
															</select>											
															<label for="visa_status_text">Visa Status </label>
														</div>
													</div>
											</div>	
											
											<div class="row">
												<div class="col s4">
													<div class="input-field">
															<input name="visa_appt_date" id="visa_appt_date" class="datepicker" type="text">												
															<label for="visa_appt_date">Visa Appointment Date </label>
													</div>
												</div>
												<div class="col s4">
													<div class="input-field">
															<input name="visa_reciev_date" id="visa_reciev_date" class="datepicker" type="text">												
															<label for="visa_reciev_date">Visa Received Date </label>
													</div>
												</div>
												<div class="col s4">
													<div class="input-field">
															<input name="visa_exp_date" id="visa_exp_date" class="datepicker" type="text">												
															<label for="visa_exp_date">Visa Expiry Date  </label>
													</div>
												</div>											
											</div>
											
											<div class="row">
												<div class="col s6">
													<label>Visa Copy File Upload </label>
													<div class="file-field input-field">
															<div class="file-field input-field">
																			<div class="btn">
																					<span>Browse</span>
																					<input type="file" name="visa_file" multiple>
																			</div>
																	<div class="file-path-wrapper">
																			<input class="file-path validate">

																			<div>
																			</div>
															
																	</div>
															</div>

															<div class="uploaded-file" id="visa_file" data-type="file"></div>																
													</div>
												</div>
												
												<div class="col s6">
													<label>Visa Approval Letter File Upload </label>
													<div class="file-field input-field">
															<div class="file-field input-field">
																			<div class="btn">
																					<span>Browse</span>
																					<input type="file" name="visa_appr_file" multiple>
																			</div>
																	<div class="file-path-wrapper">
																			<input class="file-path validate">

																			<div>
																			</div>
															
																	</div>
															</div>

															<div class="uploaded-file" id="visa_appr_file" data-type="file"></div>																
													</div>
												</div>											
											</div>
										</div>
										
										<div class="row">
											<div class="col s6">
												<div class="input-field">											
													<select name="flight_tkt_status" id="flight_tkt_status">
														<option value="Not-Applicable">Not Applicable</option>
														<option value="Received">Received</option>
														<option value="Not-Received">Not Received</option>
													</select>											
													<label for="cbt_attempt_date">Flight Ticket Status </label>
												</div>
											</div>
										</div>	
										
										<div class="row" id="flight-ticket-wrapper" style="display:none;">
											<div class="col s6">
												<label>Flight Ticket File Upload </label>
												<div class="file-field input-field">
														<div class="file-field input-field">
																		<div class="btn">
																				<span>Browse</span>
																				<input type="file" name="flight_ticket" multiple>
																		</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate">

																		<div>
																		</div>
														
																</div>
														</div>

														<div class="uploaded-file" id="flight_ticket" data-type="file"></div>																
												</div>
											</div>											
										</div>										
                                </div>									
								
                                <div class="controls">
                                                <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="visa">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                