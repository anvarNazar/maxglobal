<div id="tab_10" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Section 10 </h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">
								
                                        <div class="row">
											<div class="col s4">
												<div class="input-field">											
													<select name="max_invoice_status" id="max_invoice_status">
														<option value="Not-Applicable">Not Applicable</option>
														<option value="Submitted">Submitted</option>
														<option value="Not-Submitted">Not Submitted</option>
													</select>											
													<label for="max_invoice_status">Max Invoice Status To Employer  </label>
												</div>
											</div>
											<div class="col s4 inv-status-wrapper" style="display:none;">
												<div class="input-field">
													<select name="employer_pay" id="employer_pay">
														<option value="Not-Applicable">Not Applicable</option>
														<option value="Received">Received</option>
														<option value="Not-Received">Not Received</option>
													</select>													
													<label for="employer_pay">Employer Payment Status </label>
												</div>
											</div>
											<div class="col s4 outs-status-wrapper" style="display:none;">
												<div class="input-field">
														<input name="outstanding_pay" id="outstanding_pay" class="datepicker" type="text">												
														<label for="outstanding_pay">Outstanding Payment From Employer   </label>
												</div>
											</div>											
                                        </div>
														
                                        <div class="row inv-status-wrapper" style="display:none;">
											<div class=" col s6 ">
													<div class="input-field">
															<label>Max Invoice To Employer </label>
															<input name="max_inv_empl" id="max_inv_empl" type="text" maxlength="100">												
															
													</div>
											</div>
                                        </div>	
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">											
													<select name="sub_agents" id="sub_agents">
														<option value="Not-Applicable">Not Applicable</option>
													</select>											
													<label for="sub_agents">Sub Agents </label>
												</div>
											</div>
											<div class="col s6 sub-agent-wrapper" style="display:none;">
												<div class="input-field">
													<label>Max Invoice To Employer </label>
													<input name="inv_sub_agent" id="inv_sub_agent" type="text" maxlength="100">	
												</div>
											</div>										
                                        </div>
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<select name="sub_pay_status" id="sub_pay_status">
														<option value="">Select</option>
														<option value="PAID">PAID</option>
														<option value="UNPAID">UNPAID</option>
													</select>											
													<label for="sub_agents">Sub Agent Payment Status </label>
												</div>
											</div>											
											<div class=" col s6 " id="paid-wrapper" style="display:none;">
													<div class="input-field">
															<label>Out Standing Payment </label>
															<input name="outstanding_pay_subagent" id="outstanding_pay_subagent" type="text" maxlength="250">												
															
													</div>
											</div>
                                        </div>								
										
										<div class="row">
											<div class="col s6">
												<label>Additional Documents </label>
												<div class="file-field input-field">
														<div class="file-field input-field">
																		<div class="btn">
																				<span>Browse</span>
																				<input type="file" name="addl_docs" multiple>
																		</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate">

																		<div>
																		</div>
														
																</div>
														</div>

														<div class="uploaded-file" id="addl_docs" data-type="file"></div>																
												</div>
											</div>										
										</div>
										
										<div class="row">
											<div class="col s12">
												<div class="input-field">											
													<label>Last Communication with Candidate  </label>
													<input name="last_communication" id="last_communication" type="text" maxlength="250">												
												</div>
											</div>
										</div>										
                                </div>									
								
                                <div class="controls">
                                                <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="payments">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                