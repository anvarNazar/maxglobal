<div id="tab_1" class="tab-pane active" >
        
                   
<div class="content-head">
                                        
                                        <h4>Personal Information</h4>
                                        <div class="id" id="nurse_id">Nurse ID : <b>#12675</b></div>
                        </div>
                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane" >
                        <div class="row">
                                <div class="col s6 ipfield">
									<div class="input-field">
										<i class="material-icons prefix">account_circle</i>
										<select name="title">
											<option value="Mr">Mr</option>
											<option value="Mrs">Mrs</option>
											<option value="Ms">Ms</option>
										</select>
										<span class="helper-text" data-error="wrong" data-success="right">Title</span>
									</div>
                                </div>
                                <div class=" col s6 ipfield">
									<div class="input-field">
										<input name="first_name" required id="first_name" type="text">
										<label for="first_name">First Name</label>
						   
										<span class="helper-text" data-error="wrong"
										data-success="right">As per Passport</span>
									</div>
                                </div>								
                        </div>
                        <div class="row">
                                <div class=" col s6 ipfield">
                                                <div class="input-field">
													<i class="material-icons prefix">account_circle</i>
													<input id="middle_name" name="middle_name" type="text" class="validate">
													<label for="middle_name">Middle Name</label>
													<span class="helper-text" data-error="wrong"
													data-success="right">As per Passport</span>
                                                </div>


                                        </div>
                                        <div class=" col s6 ipfield">
                                                        <div class="input-field">
                                                                        <input id="sur_name" name="sur_name" type="text" class="validate">
                                                                        <label for="sur_name">Sur Name</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>

                        </div>
						<?php if($studentId == 0 || $studentId == '') { ?>
							<div class="row">
								<div class=" col s12 ipfield">
										<div class="input-field">
														<i class="material-icons prefix">mail</i>
														<input name="email" id="email" required type="text">
														<label for="email">Email</label>
										   
										</div>
								</div>
							</div>
						<?php } ?>
                        <div class="row">
                                <div class="input-field col s12">
                                                <i class="material-icons prefix">calendar_today</i>

                                        <input id="dob" required  name="dob" type="text" class="datepicker">
                                        <label for="dob">Date of Birth</label>
                                        <span class="helper-text" data-error="wrong"
                                        data-success="right">As per Passport</span>
                                </div>

                        </div>
						
						<div class="row">
							<div class="input-field col s12">
											<i class="material-icons prefix">home</i>
							  <textarea id="home_address" name="home_address" required class="materialize-textarea"></textarea>
							  <label for="home_address">Address</label>
							</div>
						</div>
						
						<div class="row">
							<div class="col s7 ipfield">
								<label>Upload profile image</label>
								<div class="file-field input-field">
									<div class="btn">
											<span>Browse</span>
											<input type="file">
									</div>
									<div class="file-path-wrapper">
											<input class="file-path validate" name="profile_image" type="text">

											<div>
											</div>
							
									</div>
								</div>
							</div>
						</div>						

						<div class="row">
                                <div class=" col s12 ipfield">
                                        <div class="input-field">
                                                        <input name="mobile" id="mobile" required type="number">
                                                        <label for="mobile">Mobile number </label>
                                           
                                        </div>
                                </div>
                        </div>
						<div class="row">
                                <div class=" col s12 ipfield">
                                        <div class="input-field">
                                                        <input name="nationality" id="nationality" required type="text">
                                                        <label for="nationality">Nationality </label>
                                           
                                        </div>
                                </div>
                         </div>
						<div class="col s12">
							<p>Gender</p>
								<p>
									<label>
										<input name="gender" type="radio" value="MALE" checked/>
										<span>MALE</span>
									</label>
									<label>
										<input name="gender" type="radio"  value="FEMALE" />
										<span>FEMALE</span>
									</label>
							  </p>
							  <br>
						</div>						 
                </div>
                <div class="controls">
                                <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                <!-- <a class="waves-effect waves-light btn" type="submit"><i class="material-icons left">save</i>SAVE</a> -->
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="personal_information">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success']) !!}
                        </div>
                        {{ Form::close() }}
</div>