<div id="tab_3" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>Section 3</h4>
                                                        <div class="id">Nurse ID : <b>#12675</b></div>
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'passport-add-form']) !!}
                                <div class="pane">
                               
                                        <div class="row">
                                                <div class=" col s7 ipfield">
                                                        <div class="input-field">
                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                        <input id="passport_no" name="passport_no" type="text" class="validate">
                                                                        <label for="passport_no" >Passport Number</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>

                                        </div>
                                        <br>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Upload Passport Front Page</label>
                                                        <div class="file-field input-field">
                                                                                <div class="btn">
                                                                                        <span>Browse</span>
                                                                                        <input type="file" name="image_front">
                                                                                </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate">

                                                                                <div>
                                                                                </div>
                                                                
                                                                        </div>
                                                        </div>

                                                        <div class="uploaded-file" id="image_front" data-type="file"></div>
                                                </div>
                                                <div class="col s6">
                                                                <label>Upload Passport Back Page</label>
                                                                <div class="file-field input-field">
                                                                                <div class="btn">
                                                                                        <span>Browse</span>
                                                                                        <input type="file" name="image_back">
                                                                                </div>
                                                                                <div class="file-path-wrapper">
                                                                                        <input class="file-path validate">
                                                                                      

                                                                                </div>
                                                                </div>
                                                              
                                                                <div class="uploaded-file" id="image_back" data-type="file"></div>
                                                        </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Qualifications File Upload </label>
                                                        <div class="file-field input-field">
                                                                                <div class="btn">
                                                                                        <span>Browse</span>
                                                                                        <input type="file" name="qualification_file" multiple>
                                                                                </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate">

                                                                                <div>
                                                                                </div>
                                                                
                                                                        </div>
                                                        </div>

                                                        <div class="uploaded-file" id="qualification_file" data-type="file"></div>
                                                </div>
                                                <div class="col s6">
                                                                <label>Birth Certificate File Upload </label>
                                                                <div class="file-field input-field">
                                                                                <div class="btn">
                                                                                        <span>Browse</span>
                                                                                        <input type="file" name="birth_certificate">
                                                                                </div>
                                                                                <div class="file-path-wrapper">
                                                                                        <input class="file-path validate">
                                                                                      

                                                                                </div>
                                                                </div>
                                                              
                                                                <div class="uploaded-file" id="birth_certificate" data-type="file"></div>
                                                        </div>
                                        </div>
										<div class="row">
												<div class="col s6 ipfield">
													<div class="input-field">
														<select name="nmc_registration">
															<option value="NOT">Not applicable</option>
															<option value="IELTS">IELTS/OET with required score</option>
															<option value="CBT">CBT passed</option>
															<option value="NMC">Candidate with NMC decision letter</option>
														</select>
														<label for="">NMC Registration Status</label>
													</div>
												</div>
												<div class="col s6 ipfield">
													<div class="input-field">
														<input name="ielts_passed_date" class="datepicker" required id="ielts_passed_date" type="text">
														<label for="first_name">IELTS / OET Passed Date</label>
														<span class="helper-text" data-error="wrong" data-success="right"></span>
													</div>
												</div>								
										</div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>IELTS / OET TRF File Upload  </label>
                                                        <div class="file-field input-field">
                                                                                <div class="btn">
                                                                                        <span>Browse</span>
                                                                                        <input type="file" name="ielts_file" multiple>
                                                                                </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate">

                                                                                <div>
                                                                                </div>
                                                                
                                                                        </div>
                                                        </div>

                                                        <div class="uploaded-file" id="ielts_file" data-type="file"></div>
                                                </div>
												<div class="col s6 ipfield">
													<div class="input-field">
														<input name="ielts_score" id="ielts_score" type="text">
														<label for="first_name">IELTS / OET Score</label>
														<span class="helper-text" data-error="wrong" data-success="right"></span>
													</div>
												</div>												
                                        </div>										

                                </div>
                                        <div class="controls">
                                <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                <!-- <a class="waves-effect waves-light btn" type="submit"><i class="material-icons left">save</i>SAVE</a> -->
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="passport_details">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
       
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success']) !!}
                        </div>
                        {{ Form::close() }}
</div>