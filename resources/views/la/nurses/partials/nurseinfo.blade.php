<div id="tab_2" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>Section 2</h4>
                                                        <div class="id">Nurse ID : <b>#12675</b></div>
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'nurseinfo-add-form']) !!}
                                <div class="pane">
                                        <div class="row">
											<div class="col s6 ipfield">
												<div class="input-field">
													<input id="whatsapp" name="whatsapp" type="number" class="validate">
													<label for="whatsapp" >Whatsapp Number</label>
												</div>
											</div>
											<div class="col s6 ipfield">
												<div class="input-field">
													<input id="imo_no" name="imo_no" type="number" class="validate">
													<label for="imo_no" >IMO Number</label>
												</div>
											</div>								
                                        </div>

                                        <div class="row">
											<div class="col s6 ipfield">
												<div class="input-field">
													<input id="botim_no" name="botim_no" type="number" class="validate">
													<label for="botim_no" >Botim Number</label>
												</div>
											</div>	
											<div class="col s6 ipfield">
												<div class="input-field">
													<input id="skype" name="skype" type="text">
													<label for="skype" >Skype ID </label>
												</div>
											</div>	
                                        </div>
										
										<div class="row">
                                                <div class="col s12">
                                                                <label for="">Skype ID Screenshot Upload</label>
                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Browse</span>
                                                                                <input type="file" name="skype_screenshot">
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                            <input class="file-path validate" type="text" placeholder = "Upload file" >
																			<div></div>
                                                                        </div>	
                                                                </div>
															<div class="uploaded-file" id="skype_screenshot" data-type="file"></div>
                                                </div>
                                              
                                        </div>
                                        <div class="row">
											<div class=" col s12 ">
													<p>
														Do you have any disability or other condition which might necessitate
														special arrangement or facilities ?
													</p>
													<p>
															<label>
																	<input name="disablity_condition" type="radio"
																			value="1" />
																	<span>Yes</span>
															</label>
															<label>
																	<input name="disablity_condition" type="radio"
																			value="0" />
																	<span>No</span>
															</label>
													</p>
											</div>
                                        </div>
										
                                        <div class="row" id="havdisabilty" style="display: none">
											<div class="input-field col s12">
													<textarea id="specify_disability" name="specify_disability" class="materialize-textarea"></textarea>
													<label for="specify_disability">Specify your disabilities</label>
											</div>
                                        </div>

                                        <div class="row">
                                                <div class="input-field col s6">
                                                        <input id="preferred_designation" name="preferred_designation" type="text" class="validate">
                                                        <label for="preferred_designation">Prefferred Designation</label>
                                                        
                                                </div>
                                                
                                                <div class="input-field col s6">
                                                        <input id="preferred_location" name="preferred_location" type="text" class="validate">
                                                        <label for="preferred_location">Prefferred Location</label>
                                                        
                                                </div>
                                                <div class="input-field col s6">
                                                        <input id="preferred_salary" name="preferred_salary" type="text" class="validate">
                                                        <label for="preferred_salary">Prefferred Salary</label>
                                                        
                                                </div>
                                                <div class="input-field col s6">
                                                        <input id="preferred_hours" name="preferred_hours" type="text" class="validate">
                                                        <label for="preferred_hours">Prefferred Hours</label>
                                                        
                                                </div>
                                                <div class="input-field col s6">
                                                        <input id="preferred_shifts" name="preferred_shifts" type="text" class="validate">
                                                        <label for="preferred_shifts">Prefferred Shifts</label>
                                                        
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                                <label for="">CV Upload</label>
                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Browse</span>
                                                                                <input type="file" name="cv_upload_file" id="">
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                            <input class="file-path validate" type="text" placeholder = "Upload file" >
																			<div></div>
																		</div>	
                                                                </div>
																<div class="uploaded-file" id="cv_upload_file" data-type="file"></div>
                                                </div>
                                              
                                        </div>									
                                </div>
								
                        <div class="controls">
                                <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                <!-- <a class="waves-effect waves-light btn" type="submit"><i class="material-icons left">save</i>SAVE</a> -->
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="employement_info">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
       
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success']) !!}
                        </div>
                        {{ Form::close() }}
</div>