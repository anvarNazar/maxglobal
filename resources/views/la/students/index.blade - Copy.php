@extends("la.layouts.app")

@section("contentheader_title", "Students")
@section("contentheader_description", "Students listing")
@section("section", "Students")
@section("sub_section", "Listing")
@section("htmlheader_title", "Students Listing")

@section("headerElems")
@la_access("Employees", "create")
	<a href="<?php echo url(config("laraadmin.adminRoute") . '/students/0/edit'); ?>" class="btn btn-success btn-sm pull-right" >Add Students</a>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">

@section("headerElems")
@la_access("Uploads", "create")
	<button id="AddNewUploads" class="btn btn-success btn-sm pull-right">Add New</button>
@endla_access
@endsection

	<!--<div class="box-header"></div>-->
	<div class="box-body">
<<<<<<< HEAD
		<table id="mx-sudents-lists" class="table table-bordered">
			<thead>
			<tr class="success">
				<th>No:</th>
				<th>Name</th>
				<th>Email</th>
				<th>Date-of-birth</th>
				@if($show_actions)
					<th>Actions</th>
				@endif
			</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

@endsection

@push('styles')
<<<<<<< HEAD
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#mx-sudents-lists").DataTable({
		processing: true,
        serverSide: false,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/students_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
});
</script>
@endpush
