<div id="tab_3" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>SSLC Details</h4>
                                                <div class="id">Sutdent ID : <b>#12675</b></div>
                                        </div>
                                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                        <div class="pane">
                                
                                <div class="row">
                                        <div class=" col s12 ">
                                                <div class="input-field">
                                                                <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                <input id="board_name"  name="board_name" type="text" class="validate">
                                                                <label for="board_name">Board Name</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                </div>


                                        </div>

                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="start_year" name="start_year" type="text" class="validate">
                                                                <label for="start_year">Start Year</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>

                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="end_year" name="end_year" type="text" class="validate">
                                                                        <label for="end_year">Completion Year</label>

                                                                </div>
                                        </div>


                                </div>
                                <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="language" name="language" type="text" class="validate">
                                                                        <label for="language">Medium of Instruction</label>

                                                                </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="sslcgrade" name="grade" type="text" class="validate">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="eng_score" name="eng_score" type="text" class="validate">
                                                                                <label for="eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                        </div>
                                <br>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload SSLC Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Browse</span>
                                                                        <input type="file" multiple >
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="document" data-type="file"></div>	
                                        </div>


                                              </div>




                        </div>
                        <div class="controls">
                                        <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="school">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                