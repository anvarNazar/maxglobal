<div id="tab_8" class="tab-pane" >
        
<div class="content-head">

<h4>Miscellaneous</h4>
<div class="id">Sutdent ID : <b>#12675</b></div>
</div>
{!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'misc-add-form']) !!}
               
<div class="pane">
<div class="row">
        <div class=" col s12 ">
                <p>
                                Done Any Degree in UK ?
                </p>
                <p>
                        <label>
                                <input name="is_ukdeg" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="is_ukdeg" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="ukdegdone" style="display: none">
        <div class="row">
                <div class="col s6">
                        <div class="input-field ">
                                <input id="uk_degree" name="uk_degree" type="text"class="validate">
                                <label for="">Degree Name</label>
                        </div>
                </div>
                <div class="col s6">
                        <div class="input-field ">
                                <input id="university" name="university" type="text"class="validate">
                                <label for="">University Name</label>
                        </div>
                </div>
                <div class="col s6">
                        <div class="input-field ">
                                <input id="passed_on" name="passed_on" type="text"class="validate">
                                <label for="">Year of Passing</label>
                        </div>
                </div>
        </div>
</div>

<div class="row">
                <div class=" col s12 ">
                        <p>
                                        Have Any Experience ?
                        </p>
                        <p>
                                <label>
                                        <input name="exp" type="radio"
                                                value="1" />
                                        <span>Yes</span>
                                </label>
                                <label>
                                        <input name="exp" type="radio"
                                                value="0" />
                                        <span>No</span>
                                </label>
                        </p>
                </div>
        </div>
        <div id="havexp" style="display: none">
                <div class="row">
                        <div class="col s12">
                                        <label for="">Experience Certificates File Upload</label>
                                        <div class="file-field input-field">
                                                <div class="btn">
                                                        <span>Browse</span>
                                                        <input type="file" multiple >
                                                </div>
                                                <div class="file-path-wrapper">
                                                        <input class="file-path validate" name="experience_upload" type="text" placeholder = "Upload file" >
                                                </div>
                                        </div>
										<div class="uploaded-file" id="experience_upload" data-type="file"></div>
                        </div>
                      
                </div>
        </div>
        <div class="row">
                <div class=" col s12 ">
                        <p>
                                        Marital Status
                        </p>
                        <p>
                                <label>
                                        <input name="married" type="radio"
                                                value="1" />
                                        <span>Single</span>
                                </label>
                                <label>
                                        <input name="married" type="radio"
                                                value="0" />
                                        <span>Married</span>
                                </label>
                        </p>
                </div>
        </div>
        <div class="row">
                <div class=" col s12 ">
                        <p>
                                        Applying For
                        </p>
                        <p>
                                <label>
                                        <input name="applying_for" type="radio"
                                                value="1" />
                                        <span>Single</span>
                                </label>
                                <label>
                                        <input name="applying_for" type="radio"
                                                value="0" />
                                        <span>With Dependant</span>
                                </label>
                        </p>
                </div>
        </div>


<div class="row">
        <div class=" col s12 ">
                <p>
                        Have you ever studied aboroad ?
                </p>
                <p>
                        <label>
                                <input name="studiedabroad" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="studiedabroad" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="studiedabroad" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="abroadedu" name="studiedabroad" type="text"
                                        class="validate">
                                <label for="abroadedu">Specify Your Abroad
                                        Education</label>
                        </div>
                </div>


        </div>

</div>
<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Got Any Visa Refusal Before For Any Country
                </p>
                <p>
                        <label>
                                <input name="visarefusal" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="visarefusal" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="visarefusal" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="visarefused" name="visa_refused" type="text"
                                        class="validate">
                                <label for="visarefused">Specify Your Visa
                                        Refusal</label>
                        </div>
                </div>

        </div>
</div>

<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Appeared in TOEIC in the Past
                </p>
                <p>
                        <label>
                                <input name="toeic" type="radio" value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="toeic" type="radio" value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="toeic" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="toeic" name="toeic" type="text" class="validate">
                                <label for="">Please Specify</label>
                        </div>
                </div>

        </div>
</div>


<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Got Any Criminal Convictions?
                </p>
                <p>
                        <label>
                                <input  type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input  type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="criminalrecord" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="criminal_convictions" name="criminal_convictions" type="text" class="validate">
                                <label for="">Please Specify</label>
                        </div>
                </div>

        </div>
</div>
<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Got Any Disabilities ?
                </p>
                <p>
                        <label>
                                <input name="disabilities" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="disabilities" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="disabilities" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <label for="">Please Specify</label>
                                <input id="disability" name="disability" type="text" class="validate">
                        </div>
                </div>

        </div>
</div>
<div class="row">
                <div class=" col s12 ">
                        <p>
                                        You Have Any Overseas Study History ?
                        </p>
                        <p>
                                <label>
                                        <input name="studyhistory" type="radio"
                                                value="1" />
                                        <span>Yes</span>
                                </label>
                                <label>
                                        <input name="studyhistory" type="radio"
                                                value="0" />
                                        <span>No</span>
                                </label>
                        </p>
                </div>
        </div>
        <div id="havsh" style="display: none">
                
                        <table class="striped">
                        <thead>
                          <tr>
                              <th>Country</th>
                              <th>Visa Dates</th>
                              <th>Institute </th>
                              <th>Course </th>
                              <th>Completed Course </th>
                              <th>Cas Dates </th>
                              <th>Documents
                                        (Visa Copy, CAS,
                                        Provisional
                                        Certificate,
                                        Transcript etc.)</th>




                               
                          </tr>
                        </thead>
                
                        <tbody>
                          <tr>
                            <td>
                                <div class="input-field ">
                                        <label for="">Country</label>
                                        <input id="" name="over_country" type="text" class="validate">
                                </div>
                            </td>
                        
                            <td>
                                <div class="input-field ">
                                        <label for="">visa date</label>
                                        <input id="" type="text" name="over_visa_date" class="validate">
                                </div>
                                </td>
                                <td>
                                        <div class="input-field ">
                                                <label for="">institution</label>
                                                <input id="" type="text" name="over_institution" class="validate">
                                        </div>
                                </td>
                                <td>
                                        <div class="input-field ">
                                                <label for="">Course</label>
                                                <input id="" name="over_course" type="text" class="validate">
                                        </div>
                                </td>
                                <td>
                                                <label>

                                                <input name="over_completed" type="radio"
                                                value="1" />
                                        <span>Yes</span>
                                                </label>

                                                <label>

                                        <input  type="radio"
                                                value="0" />
                                        <span>No</span>
                                                                                                                                </label>

                                </td>
                                <td>
                                        <div class="input-field ">
                                                <label for="">Cas Dates</label>
                                                <input id="" name="over_casdate" type="text" class="validate">
                                        </div>
                                </td>
                                <td>
                                                <label for="">Upload Files</label>
                                                <div class="file-field input-field">
                                                        <div class="btn">
                                                                <span>Browse</span>
                                                                <input type="file" multiple name="over_document">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                                <input class="file-path validate" type="text" placeholder = "Upload file" >
                                                        </div>
                                                </div>
                                </td>
                         </tr>
                      
                        </tbody>
                      </table> 
					  
                      <div class="ash">
                              
                                
                                <div class="ash-field">
                                        
                                <div class="input-field ">
                                        <label for="">Country</label>
                                        <input id="" name="over_country" type="text" class="validate">
                                </div>
                                </div>
                                
                                <div class="ash-field">
                                        
                                <div class="input-field ">
                                        <label for="">visa date</label>
                                        <input id="" type="text" name="over_visa_date" class="validate">
                                </div>
                                </div>
                                
                                <div class="ash-field">
                                        
                                <div class="input-field ">
                                        <label for="">institution</label>
                                        <input id="" type="text" name="over_institution" class="validate">
                                </div>
                                </div>
                                
                                <div class="ash-field">
                                        
                                <div class="input-field ">
                                        <label for="">Course</label>
                                        <input id="" name="over_course" type="text" class="validate">
                                </div>
                                </div>
                              
                                <div class="ash-field">
                                <div class="input-field ">
                                        <label for="">Cas Dates</label>
                                        <input id="" name="over_casdate" type="text" class="validate">
                                </div>  
                                </div>
                                <div class="ash-field ashw50">
                                <p>
                                       Completed Course ?
                                    </p>
                                                                <label>

                                                        <input name="over_completed" type="radio"
                                                        value="1" />
                                                        <span>Yes</span>
                                                        </label>

                                                        <label>

                                                        <input name="over_completed"  type="radio"
                                                        value="0" />
                                                        <span>No</span>
                                </div>
                                
                                
                               
                                <div class="ash-field ashw50">
        
                                        <label for="">Documents
                                        (Visa Copy, CAS,
                                        Provisional
                                        Certificate,
                                        Transcript etc.)</label>
                                                        <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Browse</span>
                                                                        <input type="file" multiple name="over_document">
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" type="text" placeholder = "Upload file" >
                                                                </div>
                                                        </div>
                                </div>
                                <div class="ash-field">

                                </div>
                      </div>
        </div>






</div>
<div class="controls">
<a class="waves-effect  btn bkbtn"><i
                class="material-icons left">navigate_before</i>Back</a>
                <input id="form_name" type="hidden" class="validate" name="type_of" value="misc">
                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">                 
                                                      {!! Form::submit( 'SAVE', ['class'=>'btn btn-success']) !!}
                                              </div>
                                              {{ Form::close() }}
        
</div>