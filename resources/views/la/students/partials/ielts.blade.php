<div id="tab_7" class="tab-pane" >
        
<div class="content-head">
                                        
                                        <h4>IELTS Application Details</h4>
                                        <div class="id">Sutdent ID : <b>#12675</b></div>
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                <div class="pane">
                        <div class="row">
                                <div class=" col s12 ">


                                        <p>
                                               Do you have IELTS Certification ?
                                        </p>
                                        <p>
                                                        <label>
                                                                        <input  name="is_certified" type="radio" value="1" />
                                                                        <span>Yes</span>
                                                                      </label>
                                                                      <label>
                                                                                <input name="is_certified" type="radio"  value="0" />
                                                                                <span>No</span>
                                                                              </label>
                                                      </p>
                                                      <br>








                                </div>

                        </div>
                        <div id="ielts-details" style="display: none">
                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="listening_score" name="listening_score" type="text" class="validate">
                                                                <label for="listening_score">Listerning Score</label>
                                                        </div>
                                        </div>

                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="reading_score" name="reading_score" type="text" class="validate">
                                                                        <label for="reading_score">Reading Score</label>

                                                                </div>
                                        </div>


                                </div>
                        <div class="row">
                                <div class="col s6">
                                        <div class="input-field ">
                                                        <input id="writing_score" name="writing_score" type="text" class="validate">
                                                        <label for="writing_score">Writing Score</label>
                                                </div>
                                </div>

                                <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="speaking_score" name="speaking_score" type="text" class="validate">
                                                                <label for="speaking_score">Speaking Score</label>

                                                        </div>
                                </div>


                        </div>

                        <br>
                        <div class="row">
                                <div class="col s6">
                                        <label>Upload Your IELTS Documents</label>
                                        <div class="file-field input-field">
                                                        <div class="btn">
                                                                <span>Browse</span>
                                                                <input type="file" multiple >
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                                <input class="file-path validate" name="ielts_doc" type="text" placeholder = "Upload file" >
                                                        </div>
                                        </div>
										<div class="uploaded-file" id="ielts_doc" data-type="file"></div>
                                </div>


                                      </div>


                                </div>


                </div>
                <div class="controls">
                                <a class="waves-effect  btn bkbtn"><i class="material-icons left">navigate_before</i>Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="ielts">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn']) !!}
                                              </div>
                                              {{ Form::close() }}
       

</div>