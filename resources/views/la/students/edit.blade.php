@extends("la.layouts.app")

@section("contentheader_title", "students")
@section("contentheader_description", "students section")
@section("section", "students")
@section("sub_section", "section")
@section("htmlheader_title", "students data")

@section("headerElems")

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif






<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">

	<input type="hidden" value="<?php echo $studentId; ?>" id="student_id">
	
    <main class="clearfix">
                        <div class="form_sidebar ">

                                <ul >
                                        <li class="active">
                                                <a  class="active" href="#tab_1" data-toggle="tab" aria-expanded="true">
                                                        <span> 1 </span>
                                                        <div>Personal Information</div>
                                                </a>
                                        </li>
                                        <li >
                                                <a  href="#tab_2" data-toggle="tab" aria-expanded="false">
                                                        <span> 2 </span>
                                                        <div> Passport Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_3" data-toggle="tab" aria-expanded="false">
                                                        <span> 3 </span>
                                                        <div>SSLC Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_4" data-toggle="tab" aria-expanded="false">
                                                        <span> 4 </span>
                                                        <div>HSE Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_5" data-toggle="tab" aria-expanded="false">
                                                        <span> 5 </span>
                                                        <div>Bachelor Degree Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_6" data-toggle="tab" aria-expanded="false">
                                                        <span> 6 </span>
                                                        <div>Master Degree Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_7" data-toggle="tab" aria-expanded="false">
                                                        <span>7</span>
                                                        <div>IELTS Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_8" data-toggle="tab" aria-expanded="false">
                                                        <span>8</span>
                                                        <div>Miscellaneous</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_9" data-toggle="tab" aria-expanded="false">
                                                        <span>9</span>
                                                        <div>Application Details</div>
                                                </a>
                                        </li>
                                </ul>
                        </div>
                        <br>
                        
                        <div class="contents tab-content" id="mx_students_view">
                        @include('la.students.partials.personal')
                        @include('la.students.partials.passport')
                        @include('la.students.partials.sslc')
                        @include('la.students.partials.hse')
                        @include('la.students.partials.degree')
                        @include('la.students.partials.master')
                        @include('la.students.partials.ielts')
                        @include('la.students.partials.misc')
                        @include('la.students.partials.applctn_details')
                        </div>
                </main>
	</div>
</div>

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
	Upload Image
			</div>
			<div class="modal-body">
			
                <form action="{{ url(config('laraadmin.adminRoute') . '/upload_files') }}" id="fm_dropzone_main" enctype="multipart/form-data" method="POST">
    {{ csrf_field() }}
    <a id="closeDZ1"><i class="fa fa-times"></i></a>
    <div class="dz-message"><i class="fa fa-cloud-upload"></i><br>Drop files here to upload</div>
</form>
				</div>
		</div>
	</div>
</div>
@endla_access

@endsection




@push('styles')

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/normalize.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/stdentsform/css/materialize.css') }}">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/main.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/stdentsform/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<style> .uploaded-file { width: 150px; height: 150px;}</style>
<script>
console.log("val",$("#data").attr("data-val"))
var bsurl = $('body').attr("bsurl");
var fm_dropzone_main = null;
var cntFiles = null;
var count=null;
var elem;
$(function () {
	$('#AddModal').modal({
		dismissible: true
	});

	@la_access("Uploads", "create")
	fm_dropzone_main = new Dropzone("#fm_dropzone_main", {
        maxFilesize: 2,
        acceptedFiles: "image/*",
        init: function() {
                this.on('addedfile', function(file) {
                if (this.files.length > 1) {
                    alert("Only 1 file allowed")
                this.removeFile(this.files[0]);
                }
                });

            this.on("complete", function(file) {
                this.removeFile(file);
            });
            this.on("success", function(file) {
                $('#AddModal').modal('open');
                loadUploadedFiles();
            });
        }
    });

    $(".btn input").on("click", function(e) {
        e.preventDefault();
       elem=$(this).closest('.file-field').find(".file-path-wrapper input");
       
    $('#AddModal').modal('open');
       // $("#fm_dropzone_main").slideDown();
    });
    $("#closeDZ1").on("click", function() {
        $('#AddModal').modal('close');
    });
	@endla_access
	
    $("body").on("click", "ul.files_container .fm_file_sel", function() {
        var upload = $(this).attr("upload");
        upload = JSON.parse(upload);
        $("#EditFileModal .modal-title").html("File: "+upload.name);
        $(".file-info-form input[name=file_id]").val(upload.id);
        $(".file-info-form input[name=filename]").val(upload.name);
        $(".file-info-form input[name=url]").val(bsurl+'/files/'+upload.hash+'/'+upload.name);
        $(".file-info-form input[name=caption]").val(upload.caption);
        $("#EditFileModal #downFileBtn").attr("href", bsurl+'/files/'+upload.hash+'/'+upload.name+"?download");
        

        @if(!config('laraadmin.uploads.private_uploads'))
        if(upload.public == "1") {
            $(".file-info-form input[name=public]").attr("checked", !0);
            $(".file-info-form input[name=public]").next().removeClass("On").addClass("Off");
        } else {
            $(".file-info-form input[name=public]").attr("checked", !1);
            $(".file-info-form input[name=public]").next().removeClass("Off").addClass("On");
        }
        @endif

        $("#EditFileModal .fileObject").empty();
        if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
            $("#EditFileModal .fileObject").append('<img src="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'">');
            $("#EditFileModal .fileObject").css("padding", "15px 0px");
        } else {
            switch (upload.extension) {
                case "pdf":
                    // TODO: Object PDF
                    $("#EditFileModal .fileObject").append('<object width="100%" height="325" data="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'"></object>');
                    $("#EditFileModal .fileObject").css("padding", "0px");
                    break;
                default:
                    $("#EditFileModal .fileObject").append('<i class="fa fa-file-text-o"></i>');
                    $("#EditFileModal .fileObject").css("padding", "15px 0px");
                    break;
            }
        }
        $("#EditFileModal").modal('show');
    });
    @if(!config('laraadmin.uploads.private_uploads') && Module::hasFieldAccess("Uploads", "public", "write"))
    $('#EditFileModal .Switch.Ajax').click(function() {
        $.ajax({
            url: "{{ url(config('laraadmin.adminRoute') . '/uploads_update_public') }}",
            method: 'POST',
            data: $("form.file-info-form").serialize(),
            success: function( data ) {
               loadUploadedFiles();
            }
        });
        
    });
    @endif
	
    loadUploadedFiles();
	
	/* get student data on edit section */
	var studentId = $("#student_id").val();
	$(".uploaded-file").addClass("hide");
	if(studentId != '0') { 
		getStdentData(studentId,'tab_1');
		$("#mx_students_view input,#mx_students_view textarea").prop("readonly", false);
		
		$('body').off('click', '.form_sidebar li a');
		$('body').on('click', '.form_sidebar li a', function() {
			
			type = $(this).attr('href');
			getStdentData(studentId,type);
		});	
	}
});

function getStdentData(studentId,type) {
	type = type.replace('#','');
	var url = '{{ url(config('laraadmin.adminRoute')) }}/students_data/' + studentId + '/' + type;

	$.ajax({
		url: url,
		data: {},
		success: function(response){
			var details = JSON.parse(response);
			$.each(details, function(key, value) {
				element = $("#"+key); elementName = $("input[name='" + key + "']");

				if(element.length > 0) {
					if(element.attr('data-type') == 'file') {
						if(value != null && value != '') {
							
							element.removeClass("hide"); // remove hide class of frame to show image 
							$(".appendedImages").remove();
							$("input[name='" + key + "']").val(value);
							var images = value.split(",");
							element.css("background-image", "url(" + images[0] + ")");
							element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
							
							if(images.length > 1) {
								for(i=1;i<=(images.length)-1;i++) {
									element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
								}
							}
						}
					} else {
						console.log(element,value);
						
						element.val(value);
						element.next().addClass('active');
					}
				} else if (elementName.attr('type') == 'radio') {
					$("input[name='" + key + "'][value='" + value + "']").prop('checked', true);
					if(value == '1') {
						$("input[name='" + key + "']").closest(".row").next().show();
					} else {
						$("input[name='" + key + "']").closest(".row").next().hide();
					}
				}
			});
		}
	});			
}

function loadUploadedFiles() {
    // load folder files
    $.ajax({
        dataType: 'json',
        url: "{{ url(config('laraadmin.adminRoute') . '/uploaded_files') }}",
        success: function ( json ) {
            console.log(json);
            cntFiles = json.uploads;
            $("ul.files_container").empty();
            if(cntFiles.length) {
				//for (var index = 0; index < cntFiles.length; index++) {
                    var element = cntFiles[(cntFiles.length)-1];
                    var li = formatFile(element);
					
                    image = '<img src='+li+'>';
					if($(elem).parent().prev().find('input').attr("multiple") == undefined) {
						$(elem).closest('div').find('img').remove();
					}
                    $(elem).closest('div').append(image);
					
					var elementNameObj = $('input[name="'+$(elem).attr('name')+'"]');
					currentValue = elementNameObj.val();
					li = (currentValue != '') ? currentValue + ',' + li : li;
                    elementNameObj.val(li);
                //}
            } else {
                $("ul.files_container").html("<div class='text-center text-danger' style='margin-top:40px;'>No Files</div>");
            }
			$('#AddModal').modal('close');
        }
    });
}
function formatFile(upload) {
    var image = '';
    var url='';
    if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
		url=bsurl+'/files/'+upload.hash+'/'+upload.name+'?s=130';
        image = '<img src='+url+'>';
    } else {
        switch (upload.extension) {
            case "pdf":
                image = '<i class="fa fa-file-pdf-o"></i>';
                break;
            default:
                image = '<i class="fa fa-file-text-o"></i>';
                break;
        }
    }
    return url;
}

</script>
@endpush



