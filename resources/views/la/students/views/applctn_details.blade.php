<div id="tab_9" class="tab-pane " >
        
<div class="content-head">
                                        <h4>Application Details</h4>
                                        <div class="id">Sutdent ID : <b>#12675</b></div>

                                </div>


                                <div class="pane">

                                        <div class="row">
                                                <div class=" col s6 ">
                                                        <div class="input-field">
															<input type="text" value="" readonly>
                                                            <label for="">Selected Course</label>
                                                        </div>
                                                </div>

                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">Selected University</label>
                                                        </div>
                                                </div>

                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <input id="" type="text" >
                                                                <label for="">Tuition Fee</label>

                                                        </div>
                                                </div>


                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">Scholarship Status</label>
                                                        </div>
                                                </div>

                                                <div class="col s6">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">University Application Status</label>

                                                        </div>
                                                </div>
                                        </div>
                                        <div class="row">

                                                <div class="col s6">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">Offer Letter Status</label>
                                                        </div>
                                                        <div id="ofl-upload" style="display: none">
                                                                <br>

                                                                <label>Uploaded Offer Letter File</label>
																<div class="file-field input-field"></div>
																<div class="uploaded-file" id="offerletter_proof" data-type="file"></div>
																<div class="file-approve-buttons hide">
																	<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="offer-letter-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
																	<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="offer-letter-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
																</div>																
                                                        </div>
                                                </div>


                                        </div>

                                        <div class="row">

                                                <div class="col s6">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="tuitionfeecheck">Tuition Fee Status</label>
                                                        </div>
                                                </div>


                                        </div>
                                        <div id="tf-paid" style="display: none">
                                                <div class="row">
                                                        <div class="col s6">

                                                                <div class="input-field ">
                                                                        <input id="" type="text" >
                                                                        <label for="">Tuition Fee Amount Paid</label>

                                                                </div>

                                                        </div>

                                                        <div class="col s6">

                                                                <div class="input-field ">
                                                                        <input id="" type="text" >
                                                                        <label for="">Balance Amount to be Paid</label>

                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">

                                                        <br>

                                                        <label>Proof of Maintenance Fund (Eg : Bank Statement, Student
                                                                Loan Letter etc.)</label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="maintenance_proof" data-type="file"></div>
                										<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="maintanence-fund-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="maintanence-fund-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>                                
												</div>

                                        </div>
                                        <div class="row">
                                                <div class="col s12">

                                                        <br>

                                                        <label> TB Screening Test Report File Upload</label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="TB_proof" data-type="file"></div>
                										<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="tb-report-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="tb-report-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>   														
                                                </div>

                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">CAS Status</label>
                                                        </div>
                                                        <div id="cas-rcd" style="display: none">
                                                                <br>

                                                                <label>Uploaded CAS Files</label>
																<div class="file-field input-field"></div>
																<div class="uploaded-file" id="cas_proof" data-type="file"></div>
																<div class="file-approve-buttons hide">
																	<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="cas-file-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
																	<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="cas-file-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
																</div>                                                          
														</div>
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">Visa Application Status</label>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s4">
                                                        <div class="input-field ">
                                                                <input id="" type="text" >
                                                                <label for="">Visa Appointment Date</label>
                                                        </div>
                                                </div>
                                                <div class="col s4">
                                                        <div class="input-field ">
                                                                <input id="" type="text" >
                                                                <label for="">Visa Appointment Time</label>
                                                        </div>
                                                </div>
                                                <div class="col s4">
                                                        <div class="input-field ">
                                                                <input id="" type="text" >
                                                                <label for="">Visa Appointment Place</label>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
															<label for="">Visa Status</label>
                                                        </div>
                                                        <div id="visa-rcd" style="display: none">
                                                                <br>
                                                                <label>Uploaded Visa Files</label>
																<div class="file-field input-field"></div>
																<div class="uploaded-file" id="" data-type="file"></div>
																<div class="file-approve-buttons hide">
																	<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="visa-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
																	<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="visa-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
																</div>																
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">

                                                        <label>Upload Flight Ticket</label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="ticket_proof" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="flight-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="flight-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>                                                
												</div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
															<label for="">Enrollment Status</label>
                                                        </div>
                                                        <div id="enrolled" style="display: none">
                                                                <br>

                                                                <div class="input-field ">
                                                                        <input id="" type="text" >
                                                                        <label for="">Enrollment Number</label>

                                                                </div>
                                                        </div>

                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">Placement Through</label>
                                                        </div>


                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
															<input type="text" value="" readonly>
                                                            <label for="">Invoice Status</label>
                                                        </div>
                                                        <div id="invoiced" style="display: none">
                                                                <br>

                                                                <label>Upload Invoice</label>
																<div class="file-field input-field"></div>
																<div class="uploaded-file" id="invoice_proof" data-type="file"></div>
																<div class="file-approve-buttons hide">
																	<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="invoice-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
																	<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="invoice-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
																</div>
                                                        </div>

                                                </div>
                                        </div>
                                        <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
																	<input type="text" value="" readonly>
                                                                    <label for="">Payment Received</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
																	<input type="text" value="" readonly>
                                                                    <label for="">Sub Agent</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
																	<input type="text" value="" readonly>
                                                                    <label for="">Invoice from Sub Agent</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
																	<input type="text" value="" readonly>
                                                                    <label for="">SubAgent Payment Status</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>

                                                <div class="row">
                                                        <div class="col s12">
                                                                <label>Additional Documents File Upload</label>
																<div class="file-field input-field"></div>
																<div class="uploaded-file" id="additional_proofs" data-type="file"></div>
																<div class="file-approve-buttons hide">
																	<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="additional-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
																	<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="additional-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
																</div>        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                                <textarea id="" class="materialize-textarea"></textarea>
                                                                                <label>Last Communication with Candidate</label>

                                                                </div>
        
                                                        </div>
                                                </div>

                                </div>
                        
</div>

                