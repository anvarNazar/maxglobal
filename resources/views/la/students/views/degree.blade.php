<div id="tab_5" class="tab-pane " >

        
<div class="content-head">
                                        
                                        <h4>Bachelor Degree Details</h4>
                                        <div class="id">Sutdent ID : <b>#12675</b></div>
                                </div>
                <div class="pane">
                        
                        <div class="row">
                                <div class=" col s12 ">


                                        <p>
                                                Have you completed your Bachelor's Degree?
                                        </p>
                                        <p>
                                                        <label>
                                                                        <input name="degreedone" type="radio" value="1" />
                                                                        <span>Yes</span>
                                                                      </label>
                                                                      <label>
                                                                                <input name="degreedone" type="radio"  value="0" />
                                                                                <span>No</span>
                                                                              </label>
                                                      </p>
                                                      <br>








                                </div>

                        </div>
                        <div id="degree-details" style="display: none">
                        <div class="row">
                                        <div class=" col s12 ">
                                                        <div class="input-field">
                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                        <input id="bachelor_board_name" name="bachelor_board_name" type="text" >
                                                                        <label for="bachelor_board_name">University Name</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>
                        </div>
                        <div class="row">
                                <div class="col s6">
                                        <div class="input-field ">
                                                        <input id="start_year_degree" name="start_year_degree" type="text" >
                                                        <label for="start_year_degree">Start Year</label>
                                                        <!-- <span class="helper-text" data-error="wrong"
                                                        data-success="right">As per Passport</span> -->
                                                </div>
                                </div>

                                <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="end_year_degree" name="end_year_degree" type="text" >
                                                                <label for="end_year_degree">Completion Year</label>

                                                        </div>
                                </div>


                        </div>
                        <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="language_degree" name="language_degree" type="text" >
                                                                <label for="language_degree">Medium of Instruction</label>

                                                        </div>
                                        </div>

                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="grade_degree" name="grade_degree"  type="text" >
                                                                        <label for="grade_degree">Percentage / Grade Obtained</label>

                                                                </div>
                                        </div>

                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="eng_score_degree" name="eng_score_degree" type="text" >
                                                                        <label for="eng_score_degree">Marks Obtained in English</label>

                                                                </div>
                                        </div>


                                </div>
                        <br>
                        <div class="row">
                                <div class="col s6">
									<label>Upload Documents</label>
									<div class="file-field input-field"></div>
									<div class="uploaded-file" id="degree_doc" data-type="file"></div>
									<div class="file-approve-buttons hide">
										<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="degree-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
										<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="degree-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
									</div>
                                </div>


                                      </div>


                                </div>


                </div>
        
        
</div>