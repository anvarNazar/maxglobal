<div id="tab_8" class="tab-pane" >
        
<div class="content-head">

<h4>Miscellaneous</h4>
<div class="id">Sutdent ID : <b>#12675</b></div>
</div>              
<div class="pane">
<div class="row">
        <div class=" col s12 ">
                <p>
                                Done Any Degree in UK ?
                </p>
                <p>
                        <label>
                                <input name="is_ukdeg" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="is_ukdeg" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="ukdegdone" style="display: none">
        <div class="row">
                <div class="col s6">
                        <div class="input-field ">
                                <input id="uk_degree" name="uk_degree" type="text">
                                <label for="">Degree Name</label>
                        </div>
                </div>
                <div class="col s6">
                        <div class="input-field ">
                                <input id="university" name="university" type="text">
                                <label for="">University Name</label>
                        </div>
                </div>
                <div class="col s6">
                        <div class="input-field ">
                                <input id="passed_on" name="passed_on" type="text">
                                <label for="">Year of Passing</label>
                        </div>
                </div>
        </div>
</div>

<div class="row">
                <div class=" col s12 ">
                        <p>
                                        Have Any Experience ?
                        </p>
                        <p>
                                <label>
                                        <input name="exp" type="radio"
                                                value="1" />
                                        <span>Yes</span>
                                </label>
                                <label>
                                        <input name="exp" type="radio"
                                                value="0" />
                                        <span>No</span>
                                </label>
                        </p>
                </div>
        </div>
        <div id="havexp" style="display: none">
                <div class="row">
                        <div class="col s12">
                                        <label for="">Experience Certificates File Upload</label>
										<div class="file-field input-field"></div>
										<div class="uploaded-file" id="" data-type="file"></div>
										<div class="file-approve-buttons hide">
											<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="misc-exper-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
											<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="misc-exper-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
										</div>												
                        </div>
                      
                </div>
        </div>
        <div class="row">
                <div class=" col s12 ">
                        <p>
                                        Marital Status
                        </p>
                        <p>
                                <label>
                                        <input name="married" type="radio"
                                                value="1" />
                                        <span>Single</span>
                                </label>
                                <label>
                                        <input name="married" type="radio"
                                                value="0" />
                                        <span>Married</span>
                                </label>
                        </p>
                </div>
        </div>
        <div class="row">
                <div class=" col s12 ">
                        <p>
                                        Applying For
                        </p>
                        <p>
                                <label>
                                        <input name="applying_for" type="radio"
                                                value="1" />
                                        <span>Single</span>
                                </label>
                                <label>
                                        <input name="applying_for" type="radio"
                                                value="0" />
                                        <span>With Dependant</span>
                                </label>
                        </p>
                </div>
        </div>


<div class="row">
        <div class=" col s12 ">
                <p>
                        Have you ever studied aboroad ?
                </p>
                <p>
                        <label>
                                <input name="studiedabroad" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="studiedabroad" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="studiedabroad" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="abroadedu" name="studiedabroad" type="text"
                                        >
                                <label for="abroadedu">Specify Your Abroad
                                        Education</label>
                        </div>
                </div>


        </div>

</div>
<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Got Any Visa Refusal Before For Any Country
                </p>
                <p>
                        <label>
                                <input name="visarefusal" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="visarefusal" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="visarefusal" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="visarefused" name="visa_refused" type="text"
                                        >
                                <label for="visarefused">Specify Your Visa
                                        Refusal</label>
                        </div>
                </div>

        </div>
</div>

<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Appeared in TOEIC in the Past
                </p>
                <p>
                        <label>
                                <input name="toeic" type="radio" value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="toeic" type="radio" value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="toeic" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="toeic" name="toeic" type="text" >
                                <label for="">Please Specify</label>
                        </div>
                </div>

        </div>
</div>


<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Got Any Criminal Convictions?
                </p>
                <p>
                        <label>
                                <input  type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input  type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="criminalrecord" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <input id="criminal_convictions" name="criminal_convictions" type="text" >
                                <label for="">Please Specify</label>
                        </div>
                </div>

        </div>
</div>
<div class="row">
        <div class=" col s12 ">
                <p>
                        Have You Got Any Disabilities ?
                </p>
                <p>
                        <label>
                                <input name="disabilities" type="radio"
                                        value="1" />
                                <span>Yes</span>
                        </label>
                        <label>
                                <input name="disabilities" type="radio"
                                        value="0" />
                                <span>No</span>
                        </label>
                </p>
        </div>
</div>
<div id="disabilities" style="display: none">
        <div class="row">
                <div class="col s12">
                        <div class="input-field ">
                                <label for="">Please Specify</label>
                                <input id="disability" name="disability" type="text" >
                        </div>
                </div>

        </div>
</div>
<div class="row">
                <div class=" col s12 ">
                        <p>
                                        You Have Any Overseas Study History ?
                        </p>
                        <p>
                                <label>
                                        <input name="studyhistory" type="radio"
                                                value="1" />
                                        <span>Yes</span>
                                </label>
                                <label>
                                        <input name="studyhistory" type="radio"
                                                value="0" />
                                        <span>No</span>
                                </label>
                        </p>
                </div>
        </div>
        <div id="havsh" style="display: none">
                
                        <table class="striped">
                        <thead>
                          <tr>
                              <th>Country</th>
                              <th>Visa Dates</th>
                              <th>Institute </th>
                              <th>Course </th>
                              <th>Completed Course </th>
                              <th>Cas Dates </th>
                              <th>Documents
                                        (Visa Copy, CAS,
                                        Provisional
                                        Certificate,
                                        Transcript etc.)</th>




                               
                          </tr>
                        </thead>
                
                        <tbody>
                          <tr>
                            <td>
                                <div class="input-field ">
                                        <label for="">Country</label>
                                        <input id="" name="over_country" type="text" >
                                </div>
                            </td>
                        
                            <td>
                                <div class="input-field ">
                                        <label for="">visa date</label>
                                        <input id="" type="text" name="over_visa_date" >
                                </div>
                                </td>
                                <td>
                                        <div class="input-field ">
                                                <label for="">institution</label>
                                                <input id="" type="text" name="over_institution" >
                                        </div>
                                </td>
                                <td>
                                        <div class="input-field ">
                                                <label for="">Course</label>
                                                <input id="" name="over_course" type="text" >
                                        </div>
                                </td>
                                <td>
                                                <label>

                                                <input name="over_completed" type="radio"
                                                value="1" />
                                        <span>Yes</span>
                                                </label>

                                                <label>

                                        <input  type="radio"
                                                value="0" />
                                        <span>No</span>
                                                                                                                                </label>

                                </td>
                                <td>
                                        <div class="input-field ">
                                                <label for="">Cas Dates</label>
                                                <input id="" name="over_casdate" type="text" >
                                        </div>
                                </td>
                                <td>
                                                <label for="">Upload Files</label>
                                                <div class="file-field input-field">
													<div class="file-path-wrapper">
													
													</div>
                                                </div>
                                </td>
                         </tr>
                      
                        </tbody>
                      </table>
        </div>






</div>
        
</div>