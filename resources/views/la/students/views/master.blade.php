<div id="tab_6" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Master Degree Details</h4>
                                                <div class="id">Sutdent ID : <b>#12675</b></div>
                                </div>
                                <div class="pane">

                                        <div class="row">
                                                <div class=" col s12 ">


                                                        <p>
                                                                Have you completed your Master's Degree?
                                                        </p>
                                                        <p>
                                                                        <label>
                                                                                        <input name="pgdone" type="radio" value="1" />
                                                                                        <span>Yes</span>
                                                                                      </label>
                                                                                      <label>
                                                                                                <input name="pgdone" type="radio"  value="0" />
                                                                                                <span>No</span>
                                                                                              </label>
                                                                      </p>
                                                                      <br>








                                                </div>

                                        </div>
                                        <div id="pg-details" style="display: none">
                                        <div class="row">
                                                        <div class=" col s12 ">
                                                                        <div class="input-field">
                                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                                        <input id="univ_name" name="univ_name" type="text">
                                                                                        <label for="pg-uni">University Name</label>
                                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                                        data-success="right">As per Passport</span> -->
                                                                        </div>
                                                        </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="pg-start-year" type="text">
                                                                        <label for="pg-start-year">Start Year</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                                </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="pg-comp-year" type="text">
                                                                                <label for="pg-comp-year">Completion Year</label>

                                                                        </div>
                                                </div>


                                        </div>
                                        <div class="row">
                                                        <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="pgmedium" type="text">
                                                                                <label for="pgmedium">Medium of Instruction</label>

                                                                        </div>
                                                        </div>
        
                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="pg-grade" type="text">
                                                                                        <label for="pg-grade">Percentage / Grade Obtained</label>
        
                                                                                </div>
                                                        </div>

                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="pg-englishmark" type="text">
                                                                                        <label for="pg-englishmark">Marks Obtained in English</label>
        
                                                                                </div>
                                                        </div>
        
        
                                                </div>
                                        <br>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Upload Master Degree Documents</label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="master_doc" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="master-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="master-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>													
                                                </div>


                                                      </div>


                                                </div>


                                </div>
                                <div class="pane2">
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
																	<input type="text" value="" readonly>
                                                                    <label for="">Country Preferred</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
																	<input type="text" name="course_pref" value="" readonly>
																	<label for="">Course Preferred</label>
                                                                </div>
                                                        <div id="course-intsd" style="display:none">
                                                                <div class="input-field ">
                                                                        <input id="" type="text">
                                                                        <label for="">Specify Your Preferred Course</label>

                                                                </div>
                                                        </div>
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
																		<input type="text" name="university_pref" value="" readonly>					
                                                                        <label for="">University Preferred</label>
                                                                </div>
                                                        <div id="uni-intsd" style="display:none">
                                                                <div class="input-field ">
                                                                        <input id="" type="text" readonly>
                                                                        <label for="">Specify Your Preferred University</label>

                                                                </div>
                                                        </div>
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <input id="" type="text" class="datepicker">
                                                                        <label for="">Course Preferred Intake</label>
                                                                </div>
                                                        </div>  
                                                </div>
                                                <div class="row">
													<div class="col s12">
														<label for="">Letter of Recommendation File Upload</label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="letter_url" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="letter-recommend-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="letter-recommend-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>														
													</div>  
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
															<label for="">Statement of Purpose File Upload</label>
															<div class="file-field input-field"></div>
															<div class="uploaded-file" id="statement_url" data-type="file"></div>
															<div class="file-approve-buttons hide">
																<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="statement-purpose-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
																<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="statement-purpose-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
															</div>															
                                                        </div>  
                                                </div>


                                </div>
                        

</div>

                