<div id="tab_3" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>SSLC Details</h4>
                                                <div class="id">Sutdent ID : <b>#12675</b></div>
                                        </div>
                        <div class="pane">
                                
                                <div class="row">
                                        <div class=" col s12 ">
                                                <div class="input-field">
                                                                <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                <input id="board_name"  name="board_name" type="text">
                                                                <label for="board_name">Board Name</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                </div>


                                        </div>

                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="start_year" name="start_year" type="text">
                                                                <label for="start_year">Start Year</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>

                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="end_year" name="end_year" type="text">
                                                                        <label for="end_year">Completion Year</label>

                                                                </div>
                                        </div>


                                </div>
                                <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="language" name="language" type="text">
                                                                        <label for="language">Medium of Instruction</label>

                                                                </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="sslcgrade" name="grade" type="text">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="eng_score" name="eng_score" type="text">
                                                                                <label for="eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                        </div>
                                <br>
                                <div class="row">
                                        <div class="col s6">
											<label>Uploaded SSLC Documents</label>
											<div class="file-field input-field"></div>
											<div class="uploaded-file" id="document" data-type="file"></div>
											<div class="file-approve-buttons hide">
												<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="sslc-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
												<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="sslc-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
											</div>   
                                        </div>
                                </div>
                        </div>
                </div>
				