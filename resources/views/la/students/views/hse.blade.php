<div id="tab_4" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>HSE Details</h4>
                                                        <div class="id">Sutdent ID : <b>#12675</b></div>
                                                </div>
                                <div class="pane">


                                        <div class="row">
                                                <div class=" col s12 ">
                                                        <div class="input-field">
                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                        <input id="board_name_hse" name="board_name_hse" type="text">
                                                                        <label for="board_name_hse">Board Name</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                        </div>


                                                </div>

                                        </div>

                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="hse_start_year" name="hse_start_year" type="text">
                                                                        <label for="hse_start_year">Start Year</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                                </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_end_year" name="hse_end_year" type="text">
                                                                                <label for="hse_end_year">Completion Year</label>

                                                                        </div>
                                                </div>


                                        </div>
                                        <div class="row">
                                                        <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_language" name="hse_language" type="text">
                                                                                <label for="hse_language">Medium of Instruction</label>

                                                                        </div>
                                                        </div>
        
                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="hse_grade" name="hse_grade" type="text">
                                                                                        <label for="hse_grade">Percentage / Grade Obtained</label>
        
                                                                                </div>
                                                        </div>

                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="hse_eng_score" name="hse_eng_score" type="text">
                                                                                        <label for="hse_eng_score">Marks Obtained in English</label>
        
                                                                                </div>
                                                        </div>
        
        
                                                </div>
                                        <br>
                                        <div class="row">
                                                <div class="col s6">
                                                    <label>Upload HSE Documents</label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="document_higher" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="hse-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="hse-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>									
                                                </div>


                                                      </div>



                                </div>
                        </div>