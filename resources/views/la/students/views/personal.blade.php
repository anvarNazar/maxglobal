<div id="tab_1" class="tab-pane active" >
        
                   
<div class="content-head">
                                        
                                        <h4>Personal Information</h4>
                                        <div class="id">Sutdent ID : <b>#12675</b></div>
                        </div>
                <div class="pane" >
                        <div class="row">
                                <div class=" col s4 ipfield">
                                        <div class="input-field">
                                                        <i class="material-icons prefix">account_circle</i>
                                                        <input name="first_name" id="first_name" type="text">
                                                        <label for="first_name">First Name</label>
                                           
                                                        <span class="helper-text" data-error="wrong"
                                                        data-success="right">As per Passport</span>
                                        </div>


                                </div>
                                <div class=" col s4 ipfield">
                                                <div class="input-field">
                                                                <input id="middle_name" name="middle_name" type="text">
                                                                <label for="middle_name">Middle Name</label>
                                                                <span class="helper-text" data-success="right">As per Passport</span>
                                                </div>


                                        </div>
                                        <div class=" col s4 ipfield">
                                                        <div class="input-field">
                                                                        <input id="sur_name" name="sur_name" type="text">
                                                                        <label for="sur_name">Sur Name</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>

                        </div>

                        <div class="row">
                                <div class="input-field col s12">
                                                <i class="material-icons prefix">calendar_today</i>

                                        <input id="dob" name="dob" type="text" class="datepicker">
                                        <label for="dob">Date of Birth</label>
                                        <span class="helper-text" data-error="wrong"
                                        data-success="right">As per Passport</span>
                                </div>

                        </div>
						  <div class="row">
									<div class="input-field col s12">
													<i class="material-icons prefix">home</i>
									  <textarea id="address" name="address" class="materialize-textarea"></textarea>
									  <label for="address">Address</label>
									</div>
								  </div>

						<div class="row">
							<div class="col s6">
								<label>Profile image</label>
								<div class="file-field input-field"></div>
								<div class="file-field input-field"></div>
								<div class="up-files clearfix">
									<div class="uploaded-file" id="profile_image" data-type="file"></div>
								</div>                                              
							</div>							
						</div>

                </div>

</div>