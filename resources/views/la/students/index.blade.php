@extends("la.layouts.app")

@section("contentheader_title", "Students")
@section("contentheader_description", "Students listing")
@section("section", "Students")
@section("sub_section", "Listing")
@section("htmlheader_title", "Students Listing")

@section("headerElems")
@la_access("Employees", "create")
	<a href="<?php echo url(config("laraadmin.adminRoute") . '/students/0/edit'); ?>" class="btn btn-success btn-sm pull-right addbtn" > <i class="icon-user-follow"></i> Add Student</a>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- <div class="box box-success">
	<div class="box-body"> -->
		<table id="mx-sudents-lists" class="table table-hover mg-table mgt2 ">
			<thead>
			<tr class="success">
				<th>No:</th>
				<th>Name</th>
				<th>Email</th>
				<!--<th>Date-of-birth</th>-->
				<th>Status</th>
				<th>Assignee</th>
				@if($show_actions)
					<th>Actions</th>
				@endif
			</tr>
			</thead>
			<tbody></tbody>
		</table>
	<!-- </div>
</div> -->

<!-- common modals -->
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Assign a consultant</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_assign') }}" id="assign-stud-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($consultants) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_email">
						<div class="form-group">
							<label for="assigned_to">Select consultant * :</label>
							<select class="form-control" name="assigned_to" required>
								<option value="">Select consultant</option>
								<?php
								foreach($consultants as $cons) {
								?>
									<option value="<?php echo $cons->id; ?>"> <?php echo $cons->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No consultants to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="universityUserModal" role="dialog" aria-labelledby="universityModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="universityModalLabel">Assign a University user</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_unv_assign') }}" id="assign-univerUser-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($universityAgents) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_unv_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_unv_email">
						<div class="form-group">
							<label for="assigned_to">Select University user * :</label>
							<select class="form-control" name="assigned_to" required>
								<option value="">Select</option>
								<?php
								foreach($universityAgents as $unv) {
								?>
									<option value="<?php echo $unv->id; ?>"> <?php echo $unv->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No University user to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
var bsurl = $('body').attr("bsurl");
var fm_dropzone_main = null;
var cntFiles = null;
$(function () {
	$("#mx-sudents-lists").DataTable({
		processing: true,
        serverSide: false,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/students_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	
	$("#assign-stud-form").validate();
	$("#assign-univerUser-form").validate();

	$('body').off('click', '.stud-assign');
	$('body').on('click', '.stud-assign', function() {
		$("#assign_user_id").val($(this).attr("data-user"));
		$("#assign_email").val($(this).attr("data-email"));
	});
	$('body').off('click', '.stud-assign-university');
	$('body').on('click', '.stud-assign-university', function() {
		$("#assign_unv_user_id").val($(this).attr("data-user"));
		$("#assign_unv_email").val($(this).attr("data-email"));
	});
});
function loadUploadedFiles() {
    // load folder files
    $.ajax({
        dataType: 'json',
        url: "{{ url(config('laraadmin.adminRoute') . '/uploaded_files') }}",
        success: function ( json ) {
            console.log("loadUploadedFiles",json);
            cntFiles = json.uploads;
            $("ul.files_container").empty();
            if(cntFiles.length) {
                for (var index = 0; index < cntFiles.length; index++) {
                    var element = cntFiles[index];
                    var li = formatFile(element);
                    $("ul.files_container").append(li);
                }
            } else {
                $("ul.files_container").html("<div class='text-center text-danger' style='margin-top:40px;'>No Files</div>");
            }
        }
    });
}
function formatFile(upload) {
    var image = '';
    if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
        image = '<img src="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'?s=130">';
    } else {
        switch (upload.extension) {
            case "pdf":
                image = '<i class="fa fa-file-pdf-o"></i>';
                break;
            default:
                image = '<i class="fa fa-file-text-o"></i>';
                break;
        }
    }
    return '<li><a class="fm_file_sel" data-toggle="tooltip" data-placement="top" title="'+upload.name+'" upload=\''+JSON.stringify(upload)+'\'>'+image+'</a></li>';
}
</script>
@endpush