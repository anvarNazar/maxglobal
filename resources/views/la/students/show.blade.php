@extends('la.layouts.app')

@section('htmlheader_title')
	Students View
@endsection


@section('main-content')
<div id="page-content" class="profile2">

	<div class="bg-primary clearfix vhead">

	<div class="vhead-wrap">


		<div class="namewrap">
			<div class="vhimg"> </div>
			<h4 class="name"><?php echo $students->first_name; ?> <?php  echo $students->sur_name; ?> <span class="apprbadge"></span></h4>
		</div>
		<div class="vhactions">
			@la_access("Employees", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/students/'.$students->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i> Edit</a><br>
			@endla_access
		</div>
		</div>
					<!-- <hr> -->
					<div class="vhbottom">
					<i class="fa fa-envelope-o"></i> <?php echo  $students->email; ?>
					<span>| </span> 
					<i class="fa fa-calendar-o"></i> <?php echo  $students->dob; ?>
					</div>


	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/students/') }}" data-toggle="tooltip" data-placement="right" title="Back to view"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Timeline</a></li>
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER") || Entrust::hasRole("STUDENT"))
			<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-key"></i> Account settings</a></li>
		@endif
		<li class=""><a role="tab" data-toggle="tab" href="#tab-notifications" data-target="#tab-notifications"><i class="fa fa-clock-o"></i> Notifications</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading" style="border-bottom:none;">
						<h4></h4>
					</div>
					<div class="panel-body">
						<!--
						<div class="form-group"><label for="name" class="col-md-2">Name :</label><div class="col-md-10 fvalue"><?php echo $students->first_name; ?> <?php echo $students->middle_name; ?> <?php  echo $students->sur_name; ?></div></div>
						<div class="form-group"><label for="email" class="col-md-2">Email :</label><div class="col-md-10 fvalue"><a href="mailto:manager@gmail.com"><?php echo $students->email; ?></a></div></div>						
						<div class="form-group"><label for="email" class="col-md-2">Email :</label><div class="col-md-10 fvalue"><a href="mailto:manager@gmail.com"><?php echo $students->email; ?></a></div></div>	
						-->
						
						<main class="clearfix">
											<div class="form_sidebar ">

													<ul >
															<li class="active">
																	<a  class="active" href="#tab_1" data-toggle="tab" aria-expanded="true">
																			<span> 1 </span>
																			<div>Personal Information</div>
																	</a>
															</li>
															<li >
																	<a  href="#tab_2" data-toggle="tab" aria-expanded="false">
																			<span> 2 </span>
																			<div> Passport Details</div>
																	</a>
															</li>
															<li>
																	<a  href="#tab_3" data-toggle="tab" aria-expanded="false">
																			<span> 3 </span>
																			<div>SSLC Details</div>
																	</a>
															</li>
															<li>
																	<a  href="#tab_4" data-toggle="tab" aria-expanded="false">
																			<span> 4 </span>
																			<div>HSE Details</div>
																	</a>
															</li>
															<li>
																	<a  href="#tab_5" data-toggle="tab" aria-expanded="false">
																			<span> 5 </span>
																			<div>Bachelor Degree Details</div>
																	</a>
															</li>
															<li>
																	<a  href="#tab_6" data-toggle="tab" aria-expanded="false">
																			<span> 6 </span>
																			<div>Master Degree Details</div>
																	</a>
															</li>
															<li>
																	<a  href="#tab_7" data-toggle="tab" aria-expanded="false">
																			<span>7</span>
																			<div>IELTS Details</div>
																	</a>
															</li>
															<li>
																	<a  href="#tab_8" data-toggle="tab" aria-expanded="false">
																			<span>8</span>
																			<div>Miscellaneous</div>
																	</a>
															</li>
															<li>
																	<a  href="#tab_9" data-toggle="tab" aria-expanded="false">
																			<span>9</span>
																			<div>Application Details</div>
																	</a>
															</li>
													</ul>
											</div>
											<br>
											
											<div class="contents tab-content" id="mx_students_view">
											@include('la.students.views.personal')
											@include('la.students.views.passport')
											@include('la.students.views.sslc')
											@include('la.students.views.hse')
											@include('la.students.views.degree')
											@include('la.students.views.master')
											@include('la.students.views.ielts')
											@include('la.students.views.misc')
											@include('la.students.views.applctn_details')
											</div>
									</main>						
						
						
					</div>
				</div>
			</div>
		</div>
		
		<!-- time-line -->
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-timeline">
			@if(Session::has('success_mail_delete'))
				<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_mail_delete') }}</p>
			@endif
			<?php
			$mailDetailswithIds = json_encode(array(),true);
			if(count($mailDetails) == 0) {
			?>
				<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No mails to show</div>
			<?php
			} else {
				$mailDetailsIds = array_column($mailDetails,'id');
				$mailDetailswithIds = json_encode(array_combine($mailDetailsIds,$mailDetails),true);
			?>
				<input type="hidden" value="<?php echo $mailDetailswithIds; ?>">
				<ul class="timeline timeline-inverse" id="mail-lists">
					<?php 
					$currentDate = '';
					foreach($mailDetails as $result) {	
						$dates = explode(' ',$result->send_date);
						$old_timestamp = strtotime($dates[1]);
						$new_time = date('h:i a', $old_timestamp);
						
						if($dates[0] != $currentDate) {
							$currentDate = $dates[0];
							$old_date_timestamp = strtotime($dates[0]);
							$new_date = date('d M Y', $old_date_timestamp);
						?>
							<li class="time-label">
								<span class="bg-red"><?php echo $new_date; ?></span>
							</li>
						<?php
						}
						?>
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php echo $new_time; ?></span>
								<h3 class="timeline-header header"><a href="javascript:void(0);"><?php echo $result->name; ?></a> has sended you an email</h3>
								<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"><?php echo $result->subject; ?></a></h3>
								<div class="timeline-body">
									<?php
									$mailContent = strip_tags($result->mail_content);
									if(strlen($mailContent) > 150)
										$mailContent = substring(0,150,$mailContent).'...';
									
									echo $mailContent;
									?>
								</div>
								<div class="timeline-footer">
									<a href="javascript:void(0);" data-url="{{ url(config('laraadmin.adminRoute') . '/updateConsultantMail/'.$result->id) }}" data-id="<?php echo $result->id; ?>" data-time="<?php echo $new_time; ?>" data-user="<?php echo $result->name; ?>" data-date="<?php echo $new_date; ?>" data-subject="<?php echo $result->subject; ?>" class="btn btn-primary btn-xs mail-read-more">Read more</a>
									<a href="javascript:void(0);" data-id="<?php echo $result->id; ?>" class="btn btn-danger btn-xs mail-delete">Delete</a>
									<form action="{{ url(config('laraadmin.adminRoute') . '/deleteConsultantMail/'.$result->id.'/'.$employee->id) }}" id="delete-mail-form-<?php echo $result->id; ?>" method="post">
										{{ csrf_field() }}
									</form>
								</div>
							</div>
						</li>
					<?php 
					}
					?>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
				<ul class="timeline timeline-inverse" id="mail-details">
					<li class="time-label">
						<span class="bg-red"><?php echo $new_date; ?></span>
						<a href="javascript:void(0);" id="mail-back" class="btn btn-primary" style="float: right;margin-right: 15px;">Back</a>
					</li>
					<li>
						<i class="fa fa-envelope bg-blue"></i>
						<div class="timeline-item">
							<span class="time"><i class="fa fa-clock-o"></i><span></span></span>
							<h3 class="timeline-header header"><a href="javascript:void(0);"></a> has sended you an email</h3>
							<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"></a></h3>
							<div class="timeline-body"></div>
							<div class="timeline-footer">							
							</div>
						</div>
					</li>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
			<?php
			}
			?>
		</div>
		
		<!-- notifications -->
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-notifications">
			@if(Session::has('success_notif_delete'))
				<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_notif_delete') }}</p>
			@endif
			<?php
			$notifDetailswithIds = json_encode(array(),true);
			if(count($notifications) == 0) {
			?>
				<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No notifications to show</div>
			<?php
			} else {
				
				// documents types array
				$docarray = array('passport-front' => 'Passport front image', 'passport-back' => 'Passport back image',
						'sslc-doc' => 'School documents', 'hse-doc' => 'Higher secondary documents','degree-doc' => 'Degree documents',
						'master-doc' => 'Master degree documents','letter-recommend-doc' => 'Letter of Recommendation',
						'statement-purpose-doc' => 'Statement of purpose','ielts-doc' => 'IELTS documents','misc-exper-doc' => 'Experience certificates',
						'invoice-doc' => 'Invoice document','flight-doc' => 'Flight ticket','cas-file-doc' => 'CAS documents',
						'tb-report-doc' => 'TB report documents','maintanence-fund-doc' => 'Maintanence fund document'
					);

				$notifDetailsIds = array_column($notifications,'id');
				$notifDetailswithIds = json_encode(array_combine($notifDetailsIds,$notifications),true);
			?>
				<input type="hidden" value="<?php echo $notifDetailswithIds; ?>">
				<ul class="timeline timeline-inverse" id="notif-lists">
					<?php 
					$currentDate = '';
					foreach($notifications as $result) {	
						$dates = explode(' ',$result->notif_dat);
						$old_timestamp = strtotime($dates[1]);
						$new_time = date('h:i a', $old_timestamp);
						
						if($dates[0] != $currentDate) {
							$currentDate = $dates[0];
							$old_date_timestamp = strtotime($dates[0]);
							$new_date = date('d M Y', $old_date_timestamp);
						?>
							<li class="time-label">
								<span class="bg-red"><?php echo $new_date; ?></span>
							</li>
						<?php
						}
						?>
						<li>
							<i class="fa fa-bell-o bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php echo $new_time; ?></span>
								<h3 class="timeline-header header"><a href="javascript:void(0);"><?php echo $result->approval_text; ?></a></h3>
								<div class="timeline-body">
									<?php
									$comments = strip_tags($result->comments);
									if(strlen($comments) > 150)
										$comments = substring(0,150,$comments).'...';
									
									echo $comments;
									?>
								</div>
								<div class="timeline-footer">
									<a href="javascript:void(0);" data-url="{{ url(config('laraadmin.adminRoute') . '/updateStudentNotif/'.$result->id) }}" data-id="<?php echo $result->id; ?>" data-time="<?php echo $new_time; ?>" data-filetype="<?php echo $result->approval_text; ?>" 
										 data-date="<?php echo $new_date; ?>" class="btn btn-primary btn-xs notif-read-more">Read more</a>
									<a href="javascript:void(0);" data-id="<?php echo $result->id; ?>" class="btn btn-danger btn-xs notif-delete">Delete</a>
									<form action="{{ url(config('laraadmin.adminRoute') . '/deleteStudentNotif/'.$result->id.'/'.$result->user_id) }}" id="delete-notif-form-<?php echo $result->id; ?>" method="post">
										{{ csrf_field() }}
									</form>
								</div>
							</div>
						</li>
					<?php 
					}
					?>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
				<ul class="timeline timeline-inverse" id="notif-details">
					<li class="time-label">
						<span class="bg-red"><?php echo $new_date; ?></span>
						<a href="javascript:void(0);" id="notif-back" class="btn btn-primary" style="float: right;margin-right: 15px;">Back</a>
					</li>
					<li>
						<i class="fa fa-envelope bg-blue"></i>
						<div class="timeline-item">
							<span class="time"><i class="fa fa-clock-o"></i><span></span></span>
							<h3 class="timeline-header header"><a href="javascript:void(0);"></a> </h3>
							<div class="timeline-body"></div>
							<div class="timeline-footer"></div>
						</div>
					</li>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
			<?php
			}
			?>
		</div>
		
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER") || Entrust::hasRole("STUDENT"))
			<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
				<div class="tab-content">
					<form action="{{ url(config('laraadmin.adminRoute') . '/changeStudentPass/'.$students->id) }}" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8">
						{{ csrf_field() }}
						<div class="panel">
							<div class="panel-default panel-heading">
								<h4>Account settings</h4>
							</div>
							<div class="panel-body">
								@if (count($errors) > 0)
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
								@if(Session::has('success_message'))
									<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_message') }}</p>
								@endif
								<div class="form-group">
									<label for="password" class=" col-md-2">Password</label>
									<div class=" col-md-10">
										<input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" autocomplete="off" required="required" data-rule-minlength="6" data-msg-minlength="Please enter at least 6 characters.">
									</div>
								</div>
								<div class="form-group">
									<label for="password_confirmation" class=" col-md-2">Retype password</label>
									<div class=" col-md-10">
										<input type="password" name="password_confirmation" value="" id="password_confirmation" class="form-control" placeholder="Retype password" autocomplete="off" required="required" data-rule-equalto="#password" data-msg-equalto="Please enter the same value again.">
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Change Password</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		@endif
	</div>
</div>

<!-- common modal -->
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Submit comment</h4>
			</div>
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" id="doc-type" value="">
					<input type="hidden" id="doc-value" value="">
					<div class="form-group">
						<label for="address">Comments :</label>
						<textarea class="form-control" placeholder="Enter comments" data-rule-maxlength="300" cols="30" rows="3" name="comments" id="mx-comments"></textarea>
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="mx-comment-close" data-dismiss="modal">Close</button>
				<input class="btn btn-success" type="submit" id="mx-comment" value="Submit">
			</div>
		</div>
	</div>
</div>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<input type="hidden" value="<?php echo $students->id; ?>" id="student_id">

@endsection

@push('styles')
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/normalize.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/stdentsform/css/materialize.css') }}">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/main.css') }}"/>
<style> .uploaded-file { width: 150px; height: 150px;}</style>
@endpush

@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
$(function () {
	
	var studentId = $("#student_id").val();
	$(".uploaded-file").addClass("hide");
	getStdentData(studentId,'tab_1');
	$("#mx_students_view input,#mx_students_view textarea").prop("readonly", true);
	//$("#mx_students_view label").addClass('active');
	
	$('body').off('click', '.form_sidebar li a');
	$('body').on('click', '.form_sidebar li a', function() {
		
		type = $(this).attr('href');
		getStdentData(studentId,type);
	});
	
	$('body').off('click', '.mx-approve-doc');
	$('body').on('click', '.mx-approve-doc', function() {		
		$("#doc-type").val($(this).attr('data-type'));
		$("#doc-value").val($(this).attr('data-val'));
	});
	$('body').off('click', '#mx-comment');
	$('body').on('click', '#mx-comment', function() {	
		
		var comments = $("#mx-comments").val();
		type = $("#doc-type").val();
		statusvalue = $("#doc-value").val();
		var url = '{{ url(config('laraadmin.adminRoute')) }}/students_doc_approve';
		
		$.ajax({
		    headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url,
			type: "POST",
			data: {'id':studentId,'comment': comments,'type':type,'statusvalue':statusvalue},
			success: function(response){
				if(response == 'SUCCESS') {
					if(statusvalue == '1') {
						$("[data-type=" + type + "]").parent().html("<span class='approve_badge btn btn-success pull-right'>APPROVED</span>");
					} else {
						$("[data-type=" + type + "]").parent().html("<span class='rejected_badge btn btn-success pull-right'>REJECTED</span>");
					}
				}
				$("#mx-comment-close").trigger('click');				
			}
		});			
	});
	
	// mail details....
	$("#mail-details").hide();
	$('body').off('click', '.mail-read-more');
	$('body').on('click', '.mail-read-more', function() {
		$("#mail-details").show(); $("#mail-lists").hide();
		var mailDate = $(this).attr('data-date');
		var mailId = $(this).attr('data-id');
		var mailContent = <?php echo $mailDetailswithIds; ?>;
		mailContent = mailContent[mailId]['mail_content'];
		var mailSubject = $(this).attr('data-subject');
		var mailFrom = $(this).attr('data-user');
		var mailTime = $(this).attr('data-time');
		var url = $(this).attr('data-url');
		
		$("#mail-details .time-label bg-red").text(mailDate);
		$("#mail-details .timeline-body").html(mailContent);
		$("#mail-details .subject a").text(mailSubject);
		$("#mail-details .header a").text(mailFrom);
		$("#mail-details .time span").text(mailTime);
		
		// update the read count of mail
		$.ajax({
		   headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url,
			type: "POST",
			data: {},
			success: function(data){
			}
		});
	});
	$('body').off('click', '#mail-back');
	$('body').on('click', '#mail-back', function() {
		$("#mail-details").hide(); $("#mail-lists").show();
	});
	$('body').off('click', '.mail-delete');
	$('body').on('click', '.mail-delete', function() {
		var mailId = $(this).attr('data-id');
		if(confirm('are you sure?')) {
			$('#delete-mail-form-'+mailId).submit();
		} else { return false; }
	});
	
	// notification details....
	$("#notif-details").hide();
	$('body').off('click', '.notif-read-more');
	$('body').on('click', '.notif-read-more', function() {
		$("#notif-details").show(); $("#notif-lists").hide();
		var notifDate = $(this).attr('data-date');
		var notifId = $(this).attr('data-id');
		var notifContent = <?php echo $notifDetailswithIds; ?>; 
		notifContent = notifContent[notifId]['comments'];
		var notiffiletype = $(this).attr('data-filetype');
		var notifTime = $(this).attr('data-time');
		var url = $(this).attr('data-url');
		
		$("#notif-details .time-label bg-red").text(notifDate);
		$("#notif-details .timeline-body").html(notifContent);
		$("#notif-details .header a").text(notiffiletype);
		$("#notif-details .time span").text(notifTime);
		
		// update the read count of mail
		$.ajax({
		   headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url,
			type: "POST",
			data: {},
			success: function(data){
			}
		});
	});
	$('body').off('click', '#notif-back');
	$('body').on('click', '#notif-back', function() {
		$("#notif-details").hide(); $("#notif-lists").show();
	});
	$('body').off('click', '.notif-delete');
	$('body').on('click', '.notif-delete', function() {
		var notifId = $(this).attr('data-id');
		if(confirm('are you sure?')) {
			$('#delete-notif-form-'+notifId).submit();
		} else { return false; }
	});
});

function getStdentData(studentId,type) {
	type = type.replace('#','');
	var url = '{{ url(config('laraadmin.adminRoute')) }}/students_data/' + studentId + '/' + type;

	$.ajax({
		url: url,
		data: {},
		success: function(response){
			var details = JSON.parse(response);
			
			$.each(details, function(key, value) {
				element = $("#"+key);  elementName = $("input[name='" + key + "']");
				
				if(element.length > 0) {
					if(element.attr('data-type') == 'file') {
						if(value != null && value != '') {
							
							element.removeClass("hide"); // remove hide class of frame to show image 
							if(details[key + "_approved"] != null) {
								var studStatus = (details[key + "_approved"] == '1') ? 'APPROVED' : 'REJECTED';
								element.next().html("<span class='approve_badge btn btn-success pull-right'>" + studStatus + "</span>");
							}
							var userType = "<?php echo Auth::user()->type; ?>";
							if(userType != 'STUDENT' && userType != 'NURSE') {
								element.parent().next().removeClass('hide'); // show approve reject button
							}
							$(".appendedImages").remove();
							var images = value.split(",");
							
							element.css("background-image", "url(" + images[0] + ")");
							element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
							
							if(images.length > 1) {
								for(i=1;i<=(images.length)-1;i++) {
									element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
								}
							}
						}
					} else {
						element.val(value);
						element.next().addClass('active');
					}
				} else if (elementName.attr('type') == 'radio') {
					$("input[name='" + key + "'][value='" + value + "']").prop('checked', true);
					if(value == '1') {
						$("input[name='" + key + "']").closest(".row").next().show();
					} else {
						$("input[name='" + key + "']").closest(".row").next().hide();
					}
				}
			});
		}
	});			
}
</script>
<script src="{{ asset('la-assets/stdentsform/js/main_view.js') }}"></script>
@endpush

