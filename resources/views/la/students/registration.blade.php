<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/registration/css/normalize.css') }}"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/registration/css/main.css') }}"/>
		<link rel="stylesheet" href="{{ asset('la-assets/registration/css/hf.css') }}">
		
        <title>MaxGlobal</title>
</head>

<body>
        <div class="wrapper">

                <header id="header" style="padding-bottom: 0px;">
                        <div class="top_bar">
                                <div class="container">
                                        <ul class="top_bar_contacts">
                                                <li>
                                                        <i class="stm-phone_13_2"></i>
                                                        <div class="top_bar_contacts_text">
                                                                <i class="material-icons prefix">phone</i>

                                                                <img alt="India" class="telimg"
                                                                        src="http://maxglobalconsultants.com/wp-content/themes/twentyseventeen-child/images/resource/india.png">
                                                                +91 9809 821 821 &nbsp; &nbsp;
                                                                <img alt="India" class="telimg teluk"
                                                                        src="http://maxglobalconsultants.com/wp-content/themes/twentyseventeen-child/images/resource/uk.png">
                                                                +44 (0) 2033719955 </div>
                                                </li>
                                                <li>
                                                        <i class="stm-env_13"></i>
                                                        <div class="top_bar_contacts_text"> <i
                                                                        class="material-icons prefix">mail</i>
                                                                <a href="mailto:info@maxglobalconsultants.com">
                                                                        info@maxglobalconsultants.com</a>
                                                        </div>
                                                </li>
                                                <li>

                                                        <div class="top_bar_contacts_text"> <i
                                                                        class="material-icons prefix">place</i>
                                                                Imperial House, 2A Heigham Road,
                                                                London, E6 2JG </div>
                                                </li>
                                        </ul>

                                </div>
                        </div>

                        <div class="header_top clearfix affix-top">
                                <div class="container">
                                        <div class="header_top_wrapper">
                                                <div class="logo media-left media-middle">
                                                        <a href="http://uat.herdzo.com/maxglobal-wp/"><img
                                                                        src="http://uat.herdzo.com/maxglobal-wp/wp-content/uploads/2019/12/Max-global-Logo1.png"
                                                                        alt="Max Global Consultants"></a>
                                                </div>
                                                <div class="top_nav media-body media-middle affix-top">
                                                        <div class="top_nav_wrapper clearfix">
                                                                <ul id="menu-menu1" class="main_menu_nav">
                                                                        <li id="menu-item-3920"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-has-children menu-item-3920">
                                                                                <a
                                                                                        href="http://uat.herdzo.com/maxglobal-wp/">home</a>
                                                                                <ul class="sub-menu">
                                                                                        <li id="menu-item-3414"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3414 stm_col_width_default stm_mega_cols_inside_default">
                                                                                                <a
                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/about-us/">About
                                                                                                        Us</a></li>
                                                                                </ul>
                                                                        </li>
                                                                        <li id="menu-item-43"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-43">
                                                                                <a
                                                                                        href="http://uat.herdzo.com/maxglobal-wp/study-destinations/">Study
                                                                                        Destinations</a>
                                                                                <ul class="sub-menu">
                                                                                        <li id="menu-item-3684"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3684 stm_col_width_default stm_mega_cols_inside_default">
                                                                                                <a
                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/study-in-australia/">Australia</a>
                                                                                        </li>
                                                                                        <li id="menu-item-3829"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3829 stm_col_width_default stm_mega_cols_inside_default">
                                                                                                <a
                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/study-in-canada/">Canada</a>
                                                                                        </li>
                                                                                        <li id="menu-item-3694"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3694 stm_col_width_default stm_mega_cols_inside_default">
                                                                                                <a
                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/study-in-new-zealand/">New
                                                                                                        Zealand</a></li>
                                                                                        <li id="menu-item-3681"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3681 stm_col_width_default stm_mega_cols_inside_default">
                                                                                                <a
                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/study-in-uk/">UK</a>
                                                                                        </li>
                                                                                </ul>
                                                                        </li>
                                                                        <li id="menu-item-3914"
                                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3914">
                                                                                <a href="#">Recruitment</a>
                                                                                <ul class="sub-menu">
                                                                                        <li id="menu-item-3421"
                                                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3421 stm_col_width_default stm_mega_cols_inside_default">
                                                                                                <a href="#">UK</a>
                                                                                                <ul class="sub-menu">
                                                                                                        <li id="menu-item-3420"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3420 stm_mega_second_col_width_default">
                                                                                                                <a
                                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/nurses/">NURSES</a>
                                                                                                        </li>
                                                                                                        <li id="menu-item-3416"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3416 stm_mega_second_col_width_default">
                                                                                                                <a
                                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/doctors/">DOCTORS</a>
                                                                                                        </li>
                                                                                                        <li id="menu-item-3424"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3424 stm_mega_second_col_width_default">
                                                                                                                <a
                                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/allied-health-care-professionals/">ALLIED
                                                                                                                        HEALTH
                                                                                                                        CARE
                                                                                                                        PROFESSIONALS</a>
                                                                                                        </li>
                                                                                                </ul>
                                                                                        </li>
                                                                                        <li id="menu-item-3422"
                                                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3422 stm_col_width_default stm_mega_cols_inside_default">
                                                                                                <a
                                                                                                        href="#">International</a>
                                                                                                <ul class="sub-menu">
                                                                                                        <li id="menu-item-3419"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3419 stm_mega_second_col_width_default">
                                                                                                                <a
                                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/international-nurses/">NURSES</a>
                                                                                                        </li>
                                                                                                        <li id="menu-item-3417"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3417 stm_mega_second_col_width_default">
                                                                                                                <a
                                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/doctors-international-recruitment/">DOCTORS</a>
                                                                                                        </li>
                                                                                                        <li id="menu-item-3423"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3423 stm_mega_second_col_width_default">
                                                                                                                <a
                                                                                                                        href="http://uat.herdzo.com/maxglobal-wp/allied-health-care-professionals-international-recruitment/">ALLIED
                                                                                                                        HEALTH
                                                                                                                        CARE
                                                                                                                        PROFESSIONALS</a>
                                                                                                        </li>
                                                                                                </ul>
                                                                                        </li>
                                                                                </ul>
                                                                        </li>
                                                                        <li id="menu-item-3918"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3916 current_page_item menu-item-3918">
                                                                                <a href="http://uat.herdzo.com/maxglobal-wp/jobs/"
                                                                                        aria-current="page">Jobs</a>
                                                                        </li>
                                                                        <li id="menu-item-3971"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3971">
                                                                                <a
                                                                                        href="http://uat.herdzo.com/maxglobal-wp/max-global-events/">Events</a>
                                                                        </li>
                                                                        <li id="menu-item-3418"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3418">
                                                                                <a
                                                                                        href="http://uat.herdzo.com/maxglobal-wp/news/">News</a>
                                                                        </li>
                                                                        <li id="menu-item-3415"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3415">
                                                                                <a
                                                                                        href="http://uat.herdzo.com/maxglobal-wp/contact-us/">Contact
                                                                                        Us</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
   </header>


                <main class="clearfix">
                      
                <div class="pgtitle">
                        <div class="container">
                                <h4>Register Now</h4>

                        </div>
                </div>                        <div class="content signup">

                @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                                        <!-- <div class="content-head">
                                        
                                                        <h4>Personal Information</h4>
                                                        <div class="id">Sutdent ID : <b>#12675</b></div>
                                        </div> -->
                                        {!! Form::open(['action' => 'LA\StudentsController@registration_save', 'id' => 'personal-add-form']) !!}
               
                                <div class="pane">
                                        <div class="row">
                                                <div class=" col s4 ipfield">
                                                        <div class="input-field">
                                                                        <i class="material-icons prefix">account_circle</i>
                                                                        <input id="first_name" name="first_name" type="text" required class="validate" value="" >
                                                                        <label for="first_name">First Name</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>
                                                <div class=" col s4 ipfield">
                                                                <div class="input-field">
                                                                                <input id="middle_name" name="middle_name" type="text" class="validate" value="" >
                                                                                <label for="middle_name">Middle Name</label>
                                                                                <span class="helper-text" data-error="wrong"
                                                                                data-success="right">As per Passport</span>
                                                                </div>
        
        
                                                        </div>
                                                        <div class=" col s4 ipfield">
                                                                        <div class="input-field">
                                                                                        <input id="sur_name" name="sur_name" type="text" class="validate" >
                                                                                        <label for="sur_name">Sur Name</label>
                                                                                        <span class="helper-text" data-error="wrong"
                                                                                        data-success="right">As per Passport</span>
                                                                        </div>
                
                
                                                                </div>

                                        </div>

                                        <div class="row">
                                                <div class="input-field col s12">
                                                                <i class="material-icons prefix">calendar_today</i>

                                                        <input id="dob" name="dob" type="text" class="datepicker" >
                                                        <label for="dob">Date of Birth</label>
                                                        <span class="helper-text" data-error="wrong"
                                                        data-success="right">As per Passport</span>
                                                </div>
                                                <div class=" col s12 ipfield">
                                                        <div class="input-field">
                                                                        <i class="material-icons prefix">mail</i>
                                                                        <input id="email" required name="email" type="text" class="validate" >
                                                                        <label for="email">E mail</label>
                                                        </div>
                                                </div>
                                                <div class=" col s12 ipfield">
                                                        <div class="input-field">
                                                                        <i class="material-icons prefix">lock</i>
                                                                        <input id="pwd" required name="password" type="password" class="validate" >
                                                                        <label for="pwd">Password</label>
                                                        </div>
                                                </div>

                                        </div>
                                        <!-- <div class="row">
                                                        <div class="input-field col s6">
                                                        <i class="material-icons prefix">email</i>
                                                          <input id="email" type="email" class="validate">
                                                          <label for="email">Email</label>
                                                        </div>
                                                      </div> -->
                                                      <div class="row">
                                                                <div class="input-field col s12">
                                                                                <i class="material-icons prefix">home</i>
                                                                  <textarea id="address" name="address" class="materialize-textarea"  ></textarea>
                                                                  <label for="address">Address</label>
                                                                </div>
                                                              </div>



                                </div>
                                <div class="controls">
                                                <!-- <a class="waves-effect waves-light btn">REGISTER</a> -->
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="personal_information">
                                             
                                {!! Form::submit( 'REGISTER', ['class'=>'waves-effect waves-light btn']) !!}
                                        </div>
                                        {{ Form::close() }}    
                        </div>
                </main>
        </div>
        <footer id="footer" class="footer style_1">

                <div class="widgets_row">
                        <div class="container">
                                <div class="footer_widgets">
                                        <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <div class="footer_logo">
                                                                <a href="http://uat.herdzo.com/maxglobal-wp/">
                                                                        <img src="http://uat.herdzo.com/maxglobal-wp/wp-content/uploads/2019/12/logo-footer.png"
                                                                                alt="Max Global Consultants">
                                                                </a>
                                                        </div>
                                                        <div class="footer_text">
                                                                <p>Max Global Consultancy is a UK based company
                                                                        specialised in recruiting highly skilled health
                                                                        care professionals, from UK and overseas, to
                                                                        meet the workforce challenges in UK. Over the
                                                                        years, we have broadened our candidate and
                                                                        client network and pride in our excellent
                                                                        customer service.</p>
                                                        </div>
                                                        <section id="custom_html-2"
                                                                class="widget_text widget widget_custom_html">
                                                                <div class="textwidget custom-html-widget"></div>
                                                        </section>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <section id="custom_html-3"
                                                                class="widget_text widget widget_custom_html">
                                                                <h4 class="widget_title no_stripe">Address (UK)</h4>
                                                                <div class="textwidget custom-html-widget">
                                                                        <div class="dmh">
                                                                        </div>

                                                                        <div class="stm_contacts_widget  style_4">
                                                                                <ul>
                                                                                        <li>
                                                                                                <i
                                                                                                class="material-icons prefix">place</i>
                                                                                                <div class="text">
                                                                                                        <strong><i><img src="http://13.234.152.70/maxglobal-wp/wp-content/themes/twentyseventeen-child/images/resource/uk.png"
                                                                                                                                alt="UK">
                                                                                                                </i></strong>
                                                                                                        <br> Suite-12 ,
                                                                                                        Imperial
                                                                                                        offices,
                                                                                                        2A Heigham road
                                                                                                        , London,<br>E6
                                                                                                        2JG
                                                                                                </div>
                                                                                        </li>
                                                                                </ul>
                                                                        </div>


                                                                        <div class="stm_contacts_widget  style_4">
                                                                                <ul>
                                                                                        <li>
                                                                                                <i
                                                                                                class="material-icons prefix">phone</i>
                                                                                                <div class="text">
                                                                                                        004420 3371
                                                                                                        9955<br>004474
                                                                                                        4767 7708
                                                                                                </div>
                                                                                        </li>
                                                                                </ul>
                                                                        </div>
                                                                </div>
                                                        </section>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <section id="custom_html-4"
                                                                class="widget_text widget widget_custom_html">
                                                                <h4 class="widget_title no_stripe">Address (IN)</h4>
                                                                <div class="textwidget custom-html-widget">
                                                                        <div class="stm_contacts_widget  style_4">
                                                                                <ul>
                                                                                        <li>
                                                                                                <i
                                                                                                class="material-icons prefix">place</i>
                                                                                                <div class="text">
                                                                                                        <strong><img src="http://13.234.152.70/maxglobal-wp/wp-content/themes/twentyseventeen-child/images/resource/india.png"
                                                                                                                        alt="India">
                                                                                                        </strong>
                                                                                                        <br> 1st Floor,
                                                                                                        Thattamparampil
                                                                                                        Building,
                                                                                                        Parthas Lane,
                                                                                                        Market Junction,
                                                                                                        Kottayam -
                                                                                                        686001
                                                                                                </div>
                                                                                        </li>
                                                                                </ul>
                                                                        </div>
                                                                        <div class="stm_contacts_widget  style_4">
                                                                                <ul>
                                                                                        <li>
                                                                                                <i
                                                                                                class="material-icons prefix">phone</i>
                                                                                                <div class="text">
                                                                                                        +91 9809 821 821
                                                                                                        <br>+91 9809 831
                                                                                                        831
                                                                                                </div>
                                                                                        </li>
                                                                                </ul>
                                                                        </div>
                                                                </div>
                                                        </section>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <section id="custom_html-5"
                                                                class="widget_text widget widget_custom_html">
                                                                <h4 class="widget_title no_stripe">Connect with us</h4>
                                                                <div class="textwidget custom-html-widget">
                                                                        <div class="stm_contacts_widget  style_4">
                                                                                <ul>
                                                                                        <li>
                                                                                                <i
                                                                                                class="material-icons prefix">mail</i>
                                                                                                <div class="text">
                                                                                                        info@maxglobalconsultants.com<br>
                                                                                                        www.maxglobalconsultants.com
                                                                                                </div>
                                                                                        </li>
                                                                                </ul>
                                                                        </div>

                                                                </div>
                                                        </section>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

                <div class="copyright_row">
                        <div class="container">
                                <div class="copyright_row_wr">
                                        <div class="copyright">
                                                © 2020 Max Global Consultants. All rights reserved. Powered by Herdzo
                                                Innovations </div>
                                </div>
                        </div>
                </div>
        </footer>

</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="{{ asset('la-assets/registration/js/main.js') }}" ></script>


</html>