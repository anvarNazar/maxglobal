
$(document).ready(function(){
    $('select').formSelect();

    $('.datepicker').datepicker({
        selectMonths: true,// Creates a dropdown to control month
        selectYears: 30 // Creates a dropdown of 15 years to control year,
    });
  });

$('[name="disablity_condition"]').change(function () {
    if ($(this).val() === "1") {
      $('#havdisabilty').show();
	} else {
      $('#havdisabilty').hide();
	}
});
$('[name="interview_status"]').change(function () {
    if ($(this).val() === "pass") {
      $('#offer-letter-wrapper').show();
	} else {
      $('#offer-letter-wrapper').hide();
	}
});
$('[name="cbt_cleared"]').change(function () {
    if ($(this).val() === "1") {
      $('#cbt-details').show();
	} else {
      $('#cbt-details').hide();
	}
});
$('[name="nmc_status"]').change(function () {
    if ($(this).val() === "1") {
      $('#nmc-details').show();
	} else {
      $('#nmc-details').hide();
	}
});
$('[name="pers_status"]').change(function () {
    if ($(this).val() === "Applicable") {
      $('#personal-stat-wrapper').show();
	} else {
      $('#personal-stat-wrapper').hide();
	}
});
$('[name="pcc_status"]').change(function () {
    if ($(this).val() === "Received") {
      $('.mx-pcc-recieved').show();
	} else {
      $('.mx-pcc-recieved').hide();
	}
});
$('[name="spons_status"]').change(function () {
    if ($(this).val() === "Received") {
      $('.cos-wrapper').show();
	} else {
      $('.cos-wrapper').hide();
	}
});
$('[name="visa_status"]').change(function () {
    if ($(this).val() === "1") {
      $('#visa-details-wrapper').show();
	} else {
      $('#visa-details-wrapper').hide();
	}
});
$('[name="flight_tkt_status"]').change(function () {
    if ($(this).val() === "Received") {
      $('#flight-ticket-wrapper').show();
	} else {
      $('#flight-ticket-wrapper').hide();
	}
});
$('[name="max_invoice_status"]').change(function () {
    if ($(this).val() === "Submitted") {
      $('.inv-status-wrapper').show();
	} else {
      $('.inv-status-wrapper').hide();
	}
});
$('[name="employer_pay"]').change(function () {
    if ($(this).val() === "Received") {
      $('.outs-status-wrapper').show();
	} else {
      $('.outs-status-wrapper').hide();
	}
});
$('[name="sub_agents"]').change(function () {
    if ($(this).val() != "Not-Applicable") {
      $('.sub-agent-wrapper').show();
	} else {
      $('.sub-agent-wrapper').hide();
	}
});
$('[name="sub_pay_status"]').change(function () {
    if ($(this).val() === "PAID") {
      $('#paid-wrapper').show();
	} else {
      $('#paid-wrapper').hide();
	}
});







