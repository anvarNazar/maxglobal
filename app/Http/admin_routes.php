<?php
Route::get('/students_registration', 'LA\StudentsController@registration');
Route::post( '/registration_save', 'LA\StudentsController@registration_save');	
Route::get('/user/verify/{token}', 'LA\StudentsController@verifyUser');
/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
//	Route::post( '/registration_save', 'LA\StudentsController@registration_save');	
//	Route::resource(config('laraadmin.adminRoute') .'/registration_save', 'LA\StudentsController@registration_save');
	Route::resource(config('laraadmin.adminRoute') .'/students_registration', 'LA\StudentsController@registration');
	//Route::resource(config('laraadmin.adminRoute') . '/students_registration', 'LA\StudentsController@registration');
	
}


//echo config('laraadmin.adminRoute');exit;	
Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	Route::resource(config('laraadmin.adminRoute') . '/user_profile/{id}/edit', 'LA\EmployeesController@edit');
	Route::resource(config('laraadmin.adminRoute') . '/user_profile/{id}/view', 'LA\EmployeesController@show');
	Route::post(config('laraadmin.adminRoute') . '/deleteEmployeeMail/{id}/{employee}', 'LA\EmployeesController@deleteMail');
	
	/* ================== Managers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/managers', 'LA\ManagersController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/managers', 'LA\ManagersController');
	Route::get(config('laraadmin.adminRoute') . '/managers_dt_ajax', 'LA\ManagersController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeManagerPass/{id}', 'LA\ManagersController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteManagersMail/{id}/{employee}', 'LA\ManagersController@deleteMail');
	
	/* ================== Consultants ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/consultants', 'LA\ConsultantsController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/consultants', 'LA\ConsultantsController');
	Route::get(config('laraadmin.adminRoute') . '/consultants_dt_ajax', 'LA\ConsultantsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeConsultantPass/{id}', 'LA\ConsultantsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/setTime/{id}', 'LA\ConsultantsController@set_time');
	Route::post(config('laraadmin.adminRoute') . '/deleteConsultantMail/{id}/{employee}', 'LA\ConsultantsController@deleteMail');
	Route::post(config('laraadmin.adminRoute') . '/updateConsultantMail/{id}', 'LA\ConsultantsController@updateRead');
	
	/* ================== Main agents ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/main_agents', 'LA\MainAgentsController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/main_agents', 'LA\MainAgentsController');
	Route::get(config('laraadmin.adminRoute') . '/mainAgents_dt_ajax', 'LA\MainAgentsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeMainagentPass/{id}', 'LA\MainAgentsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteMainAgentMail/{id}/{employee}', 'LA\MainAgentsController@deleteMail');
	
	/* ================== University users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/university_users', 'LA\UniversityUserController');
	Route::get(config('laraadmin.adminRoute') . '/universityUsers_dt_ajax', 'LA\UniversityUserController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeUniversityPass/{id}', 'LA\UniversityUserController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteUniversityuserMail/{id}/{employee}', 'LA\UniversityUserController@deleteMail');
	
	/* ================== Sub-contractors ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/sub_contractors', 'LA\SubContractorsController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/sub_contractors', 'LA\SubContractorsController');
	Route::get(config('laraadmin.adminRoute') . '/subContractors_dt_ajax', 'LA\SubContractorsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changesubContractorsPass/{id}', 'LA\SubContractorsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteSubcontractorsMail/{id}/{employee}', 'LA\SubContractorsController@deleteMail');
	
	/* ================== Employers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employer', 'LA\EmployerController');
	Route::get(config('laraadmin.adminRoute') . '/employer_dt_ajax', 'LA\EmployerController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeEmployerPass/{id}', 'LA\EmployerController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteEmployerMail/{id}/{employee}', 'LA\EmployerController@deleteMail');
	
	/* ================== Students ================== */
	Route::resource(config('laraadmin.adminRoute') . '/students', 'LA\StudentsController');
	Route::resource(config('laraadmin.adminRoute') . '/students_personal', 'LA\StudentsController@registration');
	Route::get(config('laraadmin.adminRoute') . '/students_dt_ajax', 'LA\StudentsController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/students_data/{id}/{type}', 'LA\StudentsController@studentData');
	Route::post(config('laraadmin.adminRoute') . '/students_doc_approve', 'LA\StudentsController@approveDocument');
	Route::post(config('laraadmin.adminRoute') . '/updateStudentNotif/{id}', 'LA\StudentsController@updateRead');
	Route::post(config('laraadmin.adminRoute') . '/deleteStudentNotif/{id}/{studentId}', 'LA\StudentsController@deleteMail');
	Route::post(config('laraadmin.adminRoute') . '/students_assign', 'LA\StudentsController@assignConsultant');
	Route::get(config('laraadmin.adminRoute') . '/students/approve', 
			array('uses'=>'StudentsController@update','as' => 'admin.students.approve'));
	Route::post(config('laraadmin.adminRoute') . '/students_unv_assign', 'LA\StudentsController@assignUniversityUser');
	Route::post(config('laraadmin.adminRoute') . '/changeStudentPass/{id}', 'LA\StudentsController@change_password');
	
	/* ================== Nurses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/nurses', 'LA\NursesController');
	Route::get(config('laraadmin.adminRoute') . '/nurses_dt_ajax', 'LA\NursesController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/nurse_data/{id}/{type}', 'LA\NursesController@nurseData');
	Route::post(config('laraadmin.adminRoute') . '/nurses_assign', 'LA\nurses@assignConsultant');
	Route::get(config('laraadmin.adminRoute') . '/nurses/approve', 
			array('uses'=>'NursesController@update','as' => 'admin.nurses.approve'));
	Route::post(config('laraadmin.adminRoute') . '/nurses_unv_assign', 'LA\NursesController@assignUniversityUser');
	Route::post(config('laraadmin.adminRoute') . '/updateNurseNotif/{id}', 'LA\NursesController@updateRead');
	Route::post(config('laraadmin.adminRoute') . '/deleteNurseNotif/{id}/{studentId}', 'LA\NursesController@deleteMail');
	Route::post(config('laraadmin.adminRoute') . '/nurses_doc_approve', 'LA\NursesController@approveDocument');
	Route::post(config('laraadmin.adminRoute') . '/changeNursePass/{id}', 'LA\NursesController@change_password');
	
	/* ================== Time-tracking-settings functionality page ================== */
	Route::resource(config('laraadmin.adminRoute') . '/settings/timetracking', 'LA\TimetrackingController');
	Route::resource(config('laraadmin.adminRoute') . '/settings/timetracking/edit', 'LA\TimetrackingController@show');
	Route::get(config('laraadmin.adminRoute') . '/settings/timetracking/update', 
			array('uses'=>'App\Controllers\LA\TimetrackingController@update','as' => 'timetracking.post.create'));
	
	/* ================== Mail-settings functionality page ================== */
	Route::resource(config('laraadmin.adminRoute') . '/mailsettings', 'LA\MailSettingsController');
	Route::get(config('laraadmin.adminRoute') . '/mailsettings/sendmail', 
			array('uses'=>'App\Controllers\LA\MailSettingsController@update','as' => 'mailsettings.sendmail'));
			
	/* ================== Courses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/courses', 'LA\CousesController');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');
});
