<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class CousesController extends Controller
{
	public function __construct() {
		
		if(Auth::user()->type != 'SUPER_ADMIN' && Auth::user()->type != 'MANAGER')
			return abort(403);		
	}
	
	/**
	 * Display a listing of the Employees.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$courses = DB::table('manage_courses')->orderBy('id')->get();
		return View('la.courses.index', [
			'courses' => $courses
		]);
	}
	
	public function show()
	{
		$courses = DB::table('manage_courses')->orderBy('id')->get();
		return View('la.courses.index', [
			'courses' => $courses
		]);
	}

	/**
	 * Store a newly created employee in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(count($request->all()) > 0) {
			
			$postedData = $request->all();
			$courses = DB::table('manage_courses')->find($postedData['course_id']);

			if($courses) {
				DB::table('manage_courses')->where('id', $postedData['course_id'])
					->update([
						'course_name' => $postedData['course_name'],
						'course_description' => $postedData['course_desc']
					]);
			} else {			
				DB::table('manage_courses')->insert([
					'course_name' => $postedData['course_name'],
					'course_description' => $postedData['course_desc']
				]);
			}
		}
		
		return redirect(config('laraadmin.adminRoute')."/courses");
	}

	/**
	 * Remove the specified employee from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if($id != '') {
			$courses = DB::table('manage_courses')->find($id);
			if($courses) {
				DB::table('manage_courses')->where('id', '=', $id)->delete();
			}
		}
		
		return redirect(config('laraadmin.adminRoute')."/courses");
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('employees')->select($this->listing_cols)
					->whereNull('deleted_at')->where('role',3);
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Employees');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/consultants/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Employees", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/consultants/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Employees", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.consultants.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	
}
