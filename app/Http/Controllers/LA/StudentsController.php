<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\Models\Upload;
use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class StudentsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name','mobile', 'email', 'city', 'date_birth'];
	
	public function __construct() {
		
	$path = \Request::path();

		$userTypeAccess = array('SUPER_ADMIN','MANAGER','STUDENT','CONSULTANTS','UNIVERSITY_USER');
		if ($path=="students_registration" || $path=="registration_save" || \Request::is('user/verify/*')) {
			
		}
		else{	if(!in_array(Auth::user()->type, $userTypeAccess))
			return abort(403);	
		}
		
	}

	public function index()
	{			
		$consultants = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 3)->whereNull('employees.deleted_at')->get();
		$universityAgents = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 5)->whereNull('employees.deleted_at')->get();
						
		return View('la.students.index', [
			'show_actions' => $this->show_action,
			'consultants' => $consultants,
			'universityAgents' => $universityAgents
		]);
	}

	public function registration()
	{				
		return View('la.students.registration', [
			'show_actions' => $this->show_action
		]);
	}

	public function registration_save(Request $request)
	{			
		$userId = 0;
		$studentsId=array();
	   $rndtime = 	time();
	   $rules = Module::validateRules("Employees", $request);
	  $validator = Validator::make($request->all(), $rules);
	  $this->emailId = $request->all()['email'];
	
	  $validator->after(function($validator) {
		  if ($this->emailValidation() > 0) {
			  $validator->errors()->add('email', 'Email already exists');
		  }
	  });
	  
	  if ($validator->fails()) {
		  return redirect()->back()->withErrors($validator)->withInput();
	  }
	  $data = User::create([
		  'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
		  'password' => bcrypt( $request->all()['password']),'type' => 'STUDENT','created_at' => date("Y-m-d h:i:sa"),
		  'updated_at' => date("Y-m-d h:i:sa"),'verified' => 1,
	  ]);
	  try {
		  $studentsId = DB::table('users')
		  ->select('users.id')
		  ->where('users.email', $request->all()['email'])->get();
		  // insert student role
		  $this->insertStudentRole($studentsId[0]->id);
		}
		
		//catch exception
		catch(Exception $e) {
		  $validator->errors()->add('user', 'User not  exists');
		}
		$token=sha1(time());
	    DB::table("verify_users")->insert([
			'user_id' => $studentsId[0]->id,
			'token' => $token
		  ]);
		  
		  if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
	  //	Send mail to User his new Password
		  Mail::send('emails.verify', ['user' =>$request->all()['email'], 'token' =>$token,'name'=>$request->all()['first_name']], function ($m) use ($request) {
		  //	var_dump($request);
		  	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
		  	$m->to($request->all()['email'],  $request->all()['first_name'])->subject('Verify Email');
		  });
	  } else {
		  Log::info("User created username: ".$request->all()['email']);
	  }
			
	  DB::table("data_".$request->all()['type_of'])->insert([
		array_merge(request()->except(['_token','type_of','email','student_id','password']), [ 'user_id' => $studentsId[0]->id ])

		  ]);
		return View('la.students.registration', [
			'show_actions' => $this->show_action
		]);
	}

	public function verifyUser($token)
	{
	  $verifyUser =  DB::table('verify_users')
	  ->where('token', $token)->first();
	  
	  if(isset($verifyUser) ){
		$user = DB::table('users')
		->where('users.id',$verifyUser->user_id)->first();
		
		if($user->verified) {
		  DB::table("users")
		  ->where('id', $verifyUser->user_id)
		 ->update(['verified' => 0]);
		 
		  $status = "Your e-mail is verified. You can now login.";
		} else {
		  $status = "Your e-mail is already verified. You can now login.";
		}
	  } else {
		return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
	  }
	  
	  return redirect('/login')->with('status', $status);
	}
	
	/**
	 * Show the form for editing the STUDENTS.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('la.students.edit', [
			'studentId' => $id
		]);
	}

	public function store(Request $request)
	  {
		  $is_update=false;

  		 if(Module::hasAccess("Employees", "create")) {
			 $this->userId = 0;
			 $studentsId=array();
		if($request->all()['user_id']){
				$studentsId[] = (object) array('id' => $request->all()['user_id']);
			}
		if ($this->isUserExist("data_".$request->all()['type_of'],$request->all()['user_id']) > 0) {
			$is_update=true;
		}

		switch($request->all()['type_of']){
				case "personal_information":
			// insert on user table
			$rndtime = 	time();
			 $rules = Module::validateRules("Employees", $request);
			$validator = Validator::make($request->all(), $rules);
			
			if(!$is_update){
				$this->emailId = $request->all()['email'];
				$validator->after(function($validator) {
					if ($this->emailValidation() > 0) {
						$validator->errors()->add('email', 'Email already exists');
					}
				});
				if ($validator->fails()) {
					return redirect()->back()->withErrors($validator)->withInput();
				}
				$data = User::create([
					'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
					'password' => bcrypt($rndtime),'type' => 'STUDENT','created_at' => date("Y-m-d h:i:sa"),
					'updated_at' => date("Y-m-d h:i:sa")
				]);
				try {
					$studentsId = DB::table('users')
					->select('users.id')
					->where('users.email', $request->all()['email'])->get();
					
					// insert student role
					$this->insertStudentRole($studentsId[0]->id);
				} 
			   //catch exception
			   catch(Exception $e) {
				$validator->errors()->add('user', 'User not  exists');
			   } 
				if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
				//	Send mail to User his new Password
					Mail::send('emails.send_login_cred', ['user' =>$request->all()['email'], 'token' => $request->all()['_token'],'password' => $rndtime], function ($m) use ($request) {
					//	var_dump($request);
						$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
						$m->to($request->all()['email'],  $request->all()['first_name'])->subject('LaraAdmin - Login Credentials Reset');
					});
				} else {
					Log::info("User created username: ".$request->all()['email']);
				}
			} else {
				DB::table('users')->where('id', $studentsId[0]->id)->update(['name' => $request->all()['first_name']]);		
			}
			break;
				case "passport_details":
					 $image_front="";
					 $image_back="";

					//$file_array=["image_front"=>0,"image_back"=>0];
		//			array_merge(request()->except(['_token','type_of']), ['image_front' => $image_front]);
					// Code to be executed if n=label1
					break;
				case "school":
					//$document="";
					//array_merge(request()->except(['_token','type_of']), ['document' => $document]);
					// Code to be executed if n=label2
					break;
				case "higher":
						$document="";
						array_merge(request()->except(['_token','type_of']), ['document' => $document]);
						// Code to be executed if n=label2
						break;
				case "bachelors":
							$document="";
							array_merge(request()->except(['_token','type_of']), ['document' => $document]);
							// Code to be executed if n=label2
							break;
				case "masters":
					$letter_url="";
					$statement_url="";
					array_merge(request()->except(['_token','type_of']), ['letter_url' => $letter_url,'statement_url' => $statement_url]);
					// Code to be executed if n=label2
								break;
			case "misc":
									$exprience="";
									$over_document='';
									array_merge(request()->except(['_token','type_of']), ['exprience' => $exprience,'over_document' => $over_document]);
			break;
			case "aplctndetails":
				$offerletter_proof="";
				$maintenance_proof='';
				$TB_proof="";
				$cas_proof='';
				$visa_file="";
				$ticket_proof='';
				$file_array=["offerletter_proof"=>0,"maintenance_proof"=>0,
				"TB_proof"=>0,"cas_proof"=>0,"visa_file"=>0,"ticket_proof"=>0,"invoice_proof"=>0,"additional_proofs"=>1,
			];
				array_merge(request()->except(['_token','type_of']), ['exprience' => $offerletter_proof,'over_document' => $maintenance_proof]);
			break;
			
				default:
					break;
			}
			
			//echo '<pre>'; print_r($request->all());

			if ($is_update) {
				DB::table("data_".$request->all()['type_of'])
				->where('user_id', $request->all()['user_id'])
				->update(request()->except(['_token','type_of','email','student_id']));
			} else if($studentsId[0]->id){
				DB::table("data_".$request->all()['type_of'])->insert([
				array_merge(request()->except(['_token','type_of','email','student_id']), [ 'user_id' => $studentsId[0]->id ])

				]);
			}  
		
			//return redirect(config('laraadmin.adminRoute')."/students");
	
			return redirect(config('laraadmin.adminRoute').'/students/'.$studentsId[0]->id."/edit");
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	  
	public function insertStudentRole($userId)
	{
		DB::table('role_user')->insert([
			'role_id' => config('constants.STUDENT_ID'),'user_id' => $userId
		]);		
	}
	  
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if(Auth::user()->type == 'CONSULTANTS') {
			$students = DB::table('users')
						->select('users.email')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
						->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
						->where('users.assigned_to', Auth::user()->id)
						->where('users.type', 'STUDENT')->orderBy('users.created_at', 'desc')->get();			
		} else if(Auth::user()->type == 'UNIVERSITY_USER') {
			$students = DB::table('users')
						->select('users.email')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
						->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
						->where('users.assigned_to_agents', Auth::user()->id)
						->where('users.type', 'STUDENT')->orderBy('users.created_at', 'desc')->get();				
		} else {
			$students = DB::table('users')
						->select('users.email')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
						->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
						->where('users.type', 'STUDENT')->orderBy('users.created_at', 'desc')->get();
		}
		
		$out =  array();
		$out['data'] = array();
		$out['draw'] = count($students);
		$out['recordsTotal'] = count($students);
		$out['recordsFiltered'] = count($students);
		
		foreach($students as $key => $result) {
			
			$studentData = array();
			$studentData[] = $key+1;
			$studentData[] = '<a href="'.url(config('laraadmin.adminRoute') . '/students/'.$result->id).'">'.$result->first_name.' '.$result->sur_name.'</a>';
			$studentData[] = $result->email;
			//$studentData[] = $result->dob;

			// students status
			if($result->is_approved == '1') {
				$status = '<span  class="mg-status approved">Approved</span>';
			} else if($result->is_approved == '0') {
				$status = '<span mg-status  class="mg-status rejected">Rejected</span>';
			} else {
				$status = '<span class="mg-status pending">Pending</span>';
			}
			$studentData[] = (string)$status;
			
			if($result->assigned_to === null) {
				$assigneeName = '';	
			} else {
				$assignee = DB::table('users')->leftJoin('employees', 'users.context_id', '=', 'employees.id')
							->where('users.id', '=', $result->assigned_to)->pluck('employees.name');
				$assigneeName = $assignee[0];
			}
			
			$studentData[] = ($result->assigned_to != null) ? '<a target="_blank" class="mgt-con" href="'.url(config('laraadmin.adminRoute') . '/consultants/'.$result->assigned_to).'">'.$assigneeName.'</a> ' : '';
			
			if($this->show_action) {
				$output = '';
				$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/students/'.$result->id.'/edit').'" class="btn btn-warning btn-xs editbtn" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></a>';
				$output .= '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/mailsettings?email='.$result->id.'_'.$result->email).'" data-email="'.$result->email.'" class="btn btn-warning btn-xs mailbtn" data-toggle="tooltip" title="Mail" ><i class="fa fa-envelope"></i></a>';
				
				// students assigning
				//if($result->is_approved == null && $result->assigned_to === null) {
				if($result->is_approved == null) {
					$output .= '<a title="Assign student" data-email="'.$result->email.'" data-user="'.$result->id.'" data-toggle="modal" data-target="#AddModal" href="#" class="btn btn-warning btn-xs stud-assign addcbtn" >  <i class="fa fa-user-plus"></i> <span data-toggle="tooltip" title="Assign Consutant"></span></a>';	
				} else if($result->is_approved == "1" && $result->assigned_to_agents == null) {
					$output .= '<a title="Assign to University users" data-email="'.$result->email.'" data-user="'.$result->id.'" data-toggle="modal" data-target="#universityUserModal"  href="#" class="btn btn-warning btn-xs stud-assign-university addcbtn" >  <i class="fa fa-university "></i> <span data-toggle="tooltip" title="Assign University"></span></a>';	
				}
				
				if($result->is_approved === null) {
					// student approve form
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.students.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will approve the student?');"]);
					$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
					$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
					$output .= '<input type="hidden" value="1" name="user_status">';
					$output .= ' <button title="Approve Student" class="btn btn-warning btn-xs apprbtn" data-toggle="tooltip" title="Approve" type="submit"><i class="fa fa-check"></i></button>';
					$output .= Form::close();
					// student reject form
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.students.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will reject the student?');"]);
					$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
					$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
					$output .= '<input type="hidden" value="0" name="user_status">';
					$output .= ' <button title="Reject Student" class="btn btn-danger btn-xs rejcbtn" data-toggle="tooltip" title="Reject" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				
				$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.students.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
				$output .= ' <button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" type="submit"><i class="fa fa-trash"></i></button>';
				$output .= Form::close();

				$studentData[] = (string)$output;
			}

			array_push($out['data'],$studentData);
		}
		$out = json_encode($out,true);
		
		return $out;
	}
	
	public function update(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			$email = $postData['user_email'];
			DB::table('users')->where('id', $postData['user_id'])->update([ 'is_approved' => $postData['user_status'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}
	
	/**
	 * Remove from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if($id != '') {
			$users = DB::table('users')->find($id);
			if($users) {
				DB::table('users')->where('id', $id)->update(['deleted_at' => date("Y-m-d H:i:s")]);
			}
		}
		
		return redirect()->route(config('laraadmin.adminRoute') . '.students.index');
	}
	
	public function show($id) 
	{
		$students = DB::table('users')
					->select('users.email')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
					->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('data_personal_information.middle_name')
					->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
					->whereNull('users.deleted_at')
					->where('users.id', $id)->get();		
					
		$mailDetails = DB::table('mail_settings')
					->select('mail_settings.id')->addselect('mail_settings.mail_content')->addselect('mail_settings.subject')
					->addselect('us.name')->addselect('us.type')->addselect('mail_settings.send_date')
					->leftJoin('users', 'users.id', '=', 'mail_settings.to_user')
					->leftJoin('users AS us', 'us.id', '=', 'mail_settings.from_user')
					->where('mail_settings.is_deleted', 0)
					->where('users.context_id', $id)->orderBy('mail_settings.send_date', 'desc')->get();
					
		$notifications = DB::table('notifications')->select('id')->addselect('notif_dat')->addselect('user_id')
				->addselect('doc_type')->addselect('comments')->addselect('status')->addselect('approval_text')
				->where('is_deleted', 0)->where('user_id', $id)->orderBy('notif_dat', 'desc')->get();
	
		return view('la.students.show', [
			'no_header' => true,
			'no_padding' => "no-padding",
			'students' =>!empty($students)?$students[0]:$students,
			'mailDetails' => $mailDetails, 'notifications' => $notifications
		]);
	}
	
	public function studentData($id,$type)
	{
		switch($type){
			case "tab_1":
				$table = 'data_personal_information';
				break;
			case "tab_2":
				$table = 'data_passport_details';
				break;
			case "tab_3":
				$table = 'data_school';
				break;
			case "tab_4":
				$table = 'data_higher';
				break;
			case "tab_5":
				$table = 'data_bachelors';
				break;
			case "tab_6":
				$table = 'data_masters';
				break;
			case "tab_7":
				$table = 'data_ielts';
				break;
			case "tab_8":
				$table = 'data_misc';
				break;
			case "tab_9":
				$table = 'data_aplctndetails';
				break;
			default:
				break;
		}
		$where = $table.'.user_id';
		$data = DB::table($table)->where($where, $id)->get();
		$details = (count($data) > 0) ? $data[0] : $data;

		return json_encode($details,true);
	}
	public function emailValidation()
	{
		$email = DB::table('users')
		->where('email', $this->emailId)
		->get();
				// ->whereNotIn('id', [$this->userId])
			
				
		return count($email);
	}
	
	public function approveDocument(Request $request)
	{
		if(count($request->all()) > 0) {
			$id = $request->all()['id'];
			$type = $request->all()['type'];
			$status = $request->all()['statusvalue'];
			$comments = $request->all()['comment'];
			
			switch($type){
				case "passport-front":
					$table = 'data_passport_details';  $column = "image_front_approved";
					break;
				case "passport-back":
					$table = 'data_passport_details';  $column = "image_back_approved";
					break;
				case "sslc-doc":
					$table = 'data_school';  $column = "sslc_doc_approved";
					break;
				case "hse-doc":
					$table = 'data_higher';  $column = "hsc_doc_approved";
					break;
				case "degree-doc":
					$table = 'data_bachelors';  $column = "degree_doc_approved";
					break;
				case "master-doc":
					$table = 'data_masters';  $column = "master_doc_approved";
					break;
				case "letter-recommend-doc":
					$table = 'data_masters';  $column = "recommd_file_approved";
					break;
				case "statement-purpose-doc":
					$table = 'data_masters';  $column = "purpose_file_approved";
					break;
				case "ielts-doc":
					$table = 'data_ielts';  $column = "ielts_doc";
					break;
				case "misc-exper-doc":
					$table = 'data_misc';  $column = "expr_cert_approved";
					break;
				case "invoice-doc":
					$table = 'data_aplctndetails';  $column = "invoice_doc_approved";
					break;	
				case "flight-doc":
					$table = 'data_aplctndetails';  $column = "flight_doc_approved";
					break;
				case "cas-file-doc":
					$table = 'data_aplctndetails';  $column = "cas_doc_approved";
					break;
				case "tb-report-doc":
					$table = 'data_aplctndetails';  $column = "tb_report_approved";
					break;
				case "maintanence-fund-doc":
					$table = 'data_aplctndetails';  $column = "maintainace_doc_approved";
					break;					
				default:
					break;
			}
			
			$approvalText = $this->docapprovalText($type,$status);

			DB::table($table)->where('user_id', $id)->update([$column => $status]);
			DB::table('notifications')->insert(['user_id' => $id,'user_type' => 'STUDENT','doc_type' => $type,
				'comments' => $comments,'status' => $status,'approval_text' => $approvalText]);
			
			return "SUCCESS"; 
		} else {
			return "FAILED"; 
		}
	}
	
	public function docapprovalText($type,$status)
	{
		$statusText = ($status == '1') ? 'APPROVED' : 'REJECTED';
		// documents types array
		$docarray = array('passport-front' => 'Passport front image', 'passport-back' => 'Passport back image',
				'sslc-doc' => 'School documents', 'hse-doc' => 'Higher secondary documents','degree-doc' => 'Degree documents',
				'master-doc' => 'Master degree documents','letter-recommend-doc' => 'Letter of Recommendation',
				'statement-purpose-doc' => 'Statement of purpose','ielts-doc' => 'IELTS documents','misc-exper-doc' => 'Experience certificates',
				'invoice-doc' => 'Invoice document','flight-doc' => 'Flight ticket','cas-file-doc' => 'CAS documents',
				'tb-report-doc' => 'TB report documents','maintanence-fund-doc' => 'Maintanence fund document'
			);
		$returnText = "Your ".$docarray[$type]." has been ".$statusText;
		
		return $returnText;
	}
	
	public function updateRead($id)
	{
		DB::table('notifications')->where('id', $id)->update(['is_read' => 1]);
	}
	
	/**
	 * delete notifications
	 *
	 * @param  int  $id
	 * @param  int  studentId
	*/
	public function deleteMail($id,$studentId)
	{
		DB::table('notifications')->where('id', $id)->update(['is_deleted' => 1]);
		\Session::flash('success_notif_delete', 'Notifications is deleted successfully');
		
		return redirect(config('laraadmin.adminRoute') . '/students/'.$studentId.'#tab-notifications');
	}
	
	public function isUserExist($table,$user_id)
	{
		$user = DB::table($table)
				->where('user_id', [$user_id])
				->get();
				
		return count($user);
	}
	
	public function assignConsultant(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to' => $postData['assigned_to'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}

	public function assignUniversityUser(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to_agents' => $postData['assigned_to'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}	
	
	/**
     * Change Password
     *
     * @return
     */
	public function change_password($id, Request $request) {
		
		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
			'password_confirmation' => 'required|min:6|same:password'
        ]);
		
		if ($validator->fails()) {
			return \Redirect::to(config('laraadmin.adminRoute') . '/students/'.$id.'#tab-account-settings')->withErrors($validator);
		}
		$user = User::where("id", $id)->first();
		$user->password = bcrypt($request->password);
		$user->save();
		
		\Session::flash('success_message', 'Password is successfully changed');
		
		// Send mail to User his new Password
		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
			// Send mail to User his new Password
			Mail::send('emails.send_login_cred_change', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
				$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				$m->to($user->email, $user->name)->subject(LAConfigs::getByKey('sitename').' - Login Credentials changed');
			});
		} else {
			Log::info("User change_password: username: ".$user->email." Password: ".$request->password);
		}
		
		return redirect(config('laraadmin.adminRoute') . '/students/'.$id.'#tab-account-settings');
	}
}
