<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\Models\Upload;
use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class NursesController extends Controller
{
	public $show_action = true;
	
	public function __construct() {
		
		$userTypeAccess = array('SUPER_ADMIN','MANAGER','NURSE','EMPLOYER_ROLE');
		if(!in_array(Auth::user()->type, $userTypeAccess))
			return abort(403);	
	}

	public function index()
	{			
		$consultants = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 3)->whereNull('employees.deleted_at')->get();
						
		$employers = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 7)->whereNull('employees.deleted_at')->get();

		return View('la.nurses.index', [
			'show_actions' => $this->show_action,
			'consultants' => $consultants,
			'universityAgents' => $employers
		]);
	}
	
	/**
	 * Show the form for editing the STUDENTS.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('la.nurses.edit', [
			'studentId' => $id
		]);
	}
	
	public function store(Request $request)
	{
		$is_update=false;
  		if(Module::hasAccess("Employees", "create")) {
			$this->userId = 0;
			$studentsId=array();
			if($request->all()['user_id']){
					$studentsId[] = (object) array('id' => $request->all()['user_id']);
				//	$studentsId[0]->id=$request->all()['student_id'];
				}
			if ($this->isUserExist("nurse_".$request->all()['type_of'],$request->all()['user_id']) > 0) {
				$is_update=true;
			}

			switch($request->all()['type_of']){
				case "personal_information":
					// insert on user table
					$rndtime = 	time();
					 $rules = Module::validateRules("Employees", $request);
					$validator = Validator::make($request->all(), $rules);
					
					if(!$is_update){
						$this->emailId = $request->all()['email'];
						$validator->after(function($validator) {
						if ($this->emailValidation() > 0) {
							$validator->errors()->add('email', 'Email already exists');
						}
					});
					
					if ($validator->fails()) {
						return redirect()->back()->withErrors($validator)->withInput();
					}
					
					$data = User::create([
						'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
						'password' => bcrypt($rndtime),'type' => 'NURSE','created_at' => date("Y-m-d h:i:sa"),
						'updated_at' => date("Y-m-d h:i:sa")
					]);
				
					try {
						$studentsId = DB::table('users')
						->select('users.id')
						->where('users.email', $request->all()['email'])->get();
						
						// insert nurse role
						$this->insertNurseRole($studentsId[0]->id);
					  }
					  
					  //catch exception
					  catch(Exception $e) {
						$validator->errors()->add('user', 'User not  exists');
					  }
						if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
							//	Send mail to User his new Password
							Mail::send('emails.send_login_cred', ['user' =>$request->all()['email'], 'token' => $request->all()['_token'],'password' => $rndtime], function ($m) use ($request) {
								$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
								$m->to($request->all()['email'],  $request->all()['first_name'])->subject('LaraAdmin - Login Credentials Reset');
							});
						} else {
							Log::info("User created username: ".$request->all()['email']);
						}
					} else {
						DB::table('users')->where('id', $studentsId[0]->id)->update(['name' => $request->all()['first_name']]);	
					}
					break;
				default:
					break;
			}
			if ($is_update) {
					DB::table("nurse_".$request->all()['type_of'])
					->where('user_id', $request->all()['user_id'])
					->update(request()->except(['_token','type_of','email','student_id']));
			} else if($studentsId){
				DB::table("nurse_".$request->all()['type_of'])->insert([
					array_merge(request()->except(['_token','type_of','student_id']), [ 'user_id' => $studentsId[0]->id ])
				]);
			}
			//return redirect(config('laraadmin.adminRoute')."/nurses");
			
			return redirect(config('laraadmin.adminRoute').'/nurses/'.$studentsId[0]->id."/edit");
		} else {
			return redirect(config('laraadmin.adminRoute')."/nurses/0/edit");
		}
	}
	
	public function insertNurseRole($userId)
	{
		DB::table('role_user')->insert([
			'role_id' => config('constants.NURSE_ID'),'user_id' => $userId
		]);		
	}
	  
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if(Auth::user()->type == 'CONSULTANTS') {
			$students = DB::table('users')
						->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
						->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
						->where('users.assigned_to', Auth::user()->id)
						->where('users.type', 'NURSE')->orderBy('users.created_at', 'desc')->get();			
		} else if(Auth::user()->type == 'EMPLOYER_ROLE') {
			$students = DB::table('users')
						->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
						->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
						->where('users.assigned_to_agents', Auth::user()->id)
						->where('users.type', 'NURSE')->orderBy('users.created_at', 'desc')->get();				
		} else {
			$students = DB::table('users')
						->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
						->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
						->where('users.type', 'NURSE')->orderBy('users.created_at', 'desc')->get();
		}
		
		$out =  array();
		$out['data'] = array();
		$out['draw'] = count($students);
		$out['recordsTotal'] = count($students);
		$out['recordsFiltered'] = count($students);
		
		foreach($students as $key => $result) {
			
			$studentData = array();
			$studentData[] = $key+1;
			$studentData[] = '<a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$result->id).'">'.$result->first_name.' '.$result->sur_name.'</a>';
			$studentData[] = $result->email;
			//$studentData[] = $result->dob;

			// students status
			if($result->is_approved == '1') {
				$status = '<span  class="mg-status approved">Approved</span>';
			} else if($result->is_approved == '0') {
				$status = '<span mg-status  class="mg-status rejected">Rejected</span>';
			} else {
				$status = '<span class="mg-status pending">Pending</span>';
			}
			$studentData[] = (string)$status;
			
			if($result->assigned_to === null) {
				$assigneeName = '';	
			} else {
				$assignee = DB::table('users')->leftJoin('employees', 'users.context_id', '=', 'employees.id')
							->where('users.id', '=', $result->assigned_to)->pluck('employees.name');
				$assigneeName = $assignee[0];
			}
			
			$studentData[] = ($result->assigned_to != null) ? '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/consultants/'.$result->assigned_to).'">'.$assigneeName.'</a> (Consultant)' : '';
			
			if($this->show_action) {
				$output = '';
				$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$result->id.'/edit').'" class="btn btn-warning btn-xs editbtn" ><i class="fa fa-pencil"></i></a>';
				$output .= '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/mailsettings?email='.$result->id.'_'.$result->email).'" data-email="'.$result->email.'" class="btn btn-warning btn-xs mailbtn" ><i class="fa fa-envelope"></i></a>';
				
				// students assigning
				if($result->is_approved == null) {
					$output .= '<a title="Assign nurse" data-email="'.$result->email.'" data-user="'.$result->id.'" data-toggle="modal" data-target="#AddModal" href="#" class="btn btn-warning btn-xs stud-assign addcbtn" style=""><i class="fa fa-user-plus"></i></a>';	
				} else if($result->is_approved == "1" && $result->assigned_to_agents == null) {
					$output .= '<a title="Assign to Employers" data-email="'.$result->email.'" data-user="'.$result->id.'" data-toggle="modal" data-target="#universityUserModal" href="#" class="btn btn-warning btn-xs stud-assign-university" ><i class="fa fa-user-plus"></i></a>';	
				}
				
				if($result->is_approved === null) {
					// student approve form
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.nurses.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will approve the nurse?');"]);
					$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
					$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
					$output .= '<input type="hidden" value="1" name="user_status">';
					$output .= ' <button title="Approve nurse" class="btn btn-warning btn-xs apprbtn" type="submit"><i class="fa fa-check"></i></button>';
					$output .= Form::close();
					// student reject form
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.nurses.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will reject the nurse?');"]);
					$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
					$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
					$output .= '<input type="hidden" value="0" name="user_status">';
					$output .= ' <button title="Reject nurse" class="btn btn-danger btn-xs rejcbtn" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				
				$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.nurses.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
				$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></button>';
				$output .= Form::close();

				$studentData[] = (string)$output;
			}

			array_push($out['data'],$studentData);
		}
		$out = json_encode($out,true);
		
		return $out;
	}
	
	public function update(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			$email = $postData['user_email'];
			DB::table('users')->where('id', $postData['user_id'])->update([ 'is_approved' => $postData['user_status'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/nurses');
	}
	
	/**
	 * Remove from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if($id != '') {
			$users = DB::table('users')->find($id);
			if($users) {
				DB::table('users')->where('id', $id)->update(['deleted_at' => date("Y-m-d H:i:s")]);
			}
		}
		
		return redirect()->route(config('laraadmin.adminRoute') . '.nurses.index');
	}
	
	public function show($id) 
	{
		$students = DB::table('users')
					->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
					->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('nurse_personal_information.middle_name')
					->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
					->whereNull('users.deleted_at')
					->where('users.id', $id)->get();		
					
		$mailDetails = DB::table('mail_settings')
					->select('mail_settings.id')->addselect('mail_settings.mail_content')->addselect('mail_settings.subject')
					->addselect('us.name')->addselect('us.type')->addselect('mail_settings.send_date')
					->leftJoin('users', 'users.id', '=', 'mail_settings.to_user')
					->leftJoin('users AS us', 'us.id', '=', 'mail_settings.from_user')
					->where('mail_settings.is_deleted', 0)
					->where('users.context_id', $id)->orderBy('mail_settings.send_date', 'desc')->get();
					
		$notifications = DB::table('notifications')->select('id')->addselect('notif_dat')->addselect('user_id')
				->addselect('doc_type')->addselect('comments')->addselect('status')->addselect('approval_text')
				->where('is_deleted', 0)->where('user_id', $id)->orderBy('notif_dat', 'desc')->get();
				
		return view('la.nurses.show', [
			'no_header' => true,
			'no_padding' => "no-padding",
			'students' => $students[0],
			'mailDetails' => $mailDetails, 'notifications' => $notifications
		]);
	}
	
	public function nurseData($id,$type)
	{
		switch($type){
			case "tab_1":
				$table = 'nurse_personal_information';
				break;
			case "tab_2":
				$table = 'nurse_employement_info';
				break;
			case "tab_3":
				$table = 'nurse_passport_details';
				break;
			case "tab_4":
				$table = 'nurse_agent_info';
				break;
			case "tab_5":
				$table = 'nurse_cbt';
				break;
			case "tab_6":
				$table = 'nurse_nmc';
				break;
			case "tab_7":
				$table = 'nurse_pcc_nhs';
				break;
			case "tab_8":
				$table = 'nurse_visa';
				break;
			case "tab_9":
				$table = 'nurse_joining';
			case "tab_10":
				$table = 'nurse_payments';
				break;
			default:
				break;
		}
		$where = $table.'.user_id';
		$data = DB::table($table)->where($where, $id)->get();
		$details = (count($data) > 0) ? $data[0] : $data;

		return json_encode($details,true);
	}
	public function emailValidation()
	{
		$email = DB::table('users')
				->whereNotIn('id', [$this->userId])
				->where('email', $this->emailId)
				->get();
				
		return count($email);
	}
	
	public function approveDocument(Request $request)
	{
		if(count($request->all()) > 0) {
			$id = $request->all()['id'];
			$type = $request->all()['type'];
			$status = $request->all()['statusvalue'];
			$comments = $request->all()['comment'];
			
			switch($type){
				case "image_front":
				case "image_back":
				case "qualification-file":
				case "ielts_file":
					$table = 'nurse_passport_details';
					break;
				case "cv_upload_file":
					$table = 'nurse_employement_info';   
					break;
				case "visa_file":
				case "visa_appr_file":
				case "flight_ticket":
					$table = 'nurse_visa';   
					break;
				case "appl_form":
				case "refer_file":
				case "pers_stat_file":
				case "tb_file_upload":
				case "pcc_file_upload":
				case "spons_file_upload":
					$table = 'nurse_pcc_nhs';   
					break;
				case "nmc_file":
					$table = 'nurse_nmc';
					break;	
				case "offer_letter_file":
					$table = 'nurse_agent_info';
					break;
				case "cbt_score_upload":
					$table = 'nurse_cbt';
					break;				
				default:
					break;
			}
			$column = $type."_approval";
			$approvalText = $this->docapprovalText($type,$status);

			DB::table($table)->where('user_id', $id)->update([$column => $status]);
			DB::table('notifications')->insert(['user_id' => $id,'user_type' => 'STUDENT','doc_type' => $type,
				'comments' => $comments,'status' => $status,'approval_text' => $approvalText]);
			
			return "SUCCESS"; 
		} else {
			return "FAILED"; 
		}
	}
	
	public function docapprovalText($type,$status)
	{
		$statusText = ($status == '1') ? 'APPROVED' : 'REJECTED';
		// documents types array
		$docarray = array('image_front' => 'Passport front image', 'image_back' => 'Passport back image',
				'qualification-file' => 'Qualification file', 'ielts_file' => 'IELTS file',
				'cv_upload_file' => 'CV file', 'visa_file' => 'Visa doc',
				'visa_appr_file' => 'Visa approval doc', 'flight_ticket' => 'Flight ticket',
				'appl_form' => 'Application form', 'refer_file' => 'Reference doc',
				'pers_stat_file' => 'personal status doc', 'tb_file_upload' => 'TB doc',
				'pcc_file_upload' => 'PCC image', 'spons_file_upload' => 'Sponsor doc',
				'nmc_file' => 'NMC doc', 'offer_letter_file' => 'Offerr letter','cbt_score_upload' => 'CBT score doc',
			);
		$returnText = "Your ".$docarray[$type]." has been ".$statusText;
		
		return $returnText;
	}
	
	public function updateRead($id)
	{
		DB::table('notifications')->where('id', $id)->update(['is_read' => 1]);
	}
	
	/**
	 * delete notifications
	 *
	 * @param  int  $id
	 * @param  int  studentId
	*/
	public function deleteMail($id,$studentId)
	{
		DB::table('notifications')->where('id', $id)->update(['is_deleted' => 1]);
		\Session::flash('success_notif_delete', 'Notifications is deleted successfully');
		
		return redirect(config('laraadmin.adminRoute') . '/nurses/'.$studentId.'#tab-notifications');
	}
	
	public function isUserExist($table,$user_id)
	{
		$user = DB::table($table)
				->where('user_id', [$user_id])
				->get();
				
		return count($user);
	}
	
	public function assignConsultant(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to' => $postData['assigned_to'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/nurses');
	}

	// assign to employer
	public function assignUniversityUser(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to_agents' => $postData['assigned_to'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/nurses');
	}	
	
	/**
     * Change Password
     *
     * @return
     */
	public function change_password($id, Request $request) {
		
		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
			'password_confirmation' => 'required|min:6|same:password'
        ]);
		
		if ($validator->fails()) {
			return \Redirect::to(config('laraadmin.adminRoute') . '/nurses/'.$id.'#tab-account-settings')->withErrors($validator);
		}
		$user = User::where("id", $id)->first();
		$user->password = bcrypt($request->password);
		$user->save();
		
		\Session::flash('success_message', 'Password is successfully changed');
		
		// Send mail to User his new Password
		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
			// Send mail to User his new Password
			Mail::send('emails.send_login_cred_change', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
				$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				$m->to($user->email, $user->name)->subject(LAConfigs::getByKey('sitename').' - Login Credentials changed');
			});
		} else {
			Log::info("User change_password: username: ".$user->email." Password: ".$request->password);
		}
		
		return redirect(config('laraadmin.adminRoute') . '/nurses/'.$id.'#tab-account-settings');
	}
}
