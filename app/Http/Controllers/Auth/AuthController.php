<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Employee;
use App\Role;
use Validator;
use Eloquent;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
    */
    protected $redirectTo = '/admin';
	
    /**
     * Where to redirect users after logout.
     *
     * @var string
    */	
	protected $redirectAfterLogout = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }
   
   
	public function authenticated(Request $request,$user)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
			
			if(Auth::user()->type == 'CONSULTANTS') {

				$timeSettings = DB::table('time_tracking_settings')->first();
				if(count($timeSettings) > 0) {
					$currentDate = new \DateTime(date("d-m-Y"));
					$timeTrack = DB::table('user_time_track')->where('employee_id', Auth::user()->context_id)->get();
					$datetime2 = new \DateTime(str_replace('/','-',$timeTrack[0]->track_date));
					$interval = $currentDate->diff($datetime2);
						
					if(count($timeTrack) > 0 && $interval->format('%a') == '0') {
						if(time() < strtotime($timeTrack->from_time) || time() > strtotime($timeTrack->to_time)) {
							Auth::logout();
						}
					} else if($timeSettings->status != 'ANYTIME') {
						$currentDay = date("N"); $daysArray = array(6,7);
						if( in_array($currentDay, $daysArray) || time() < strtotime($timeSettings->from_time) || time() > strtotime($timeSettings->to_time) ) {
							Auth::logout();
						}
					}
				}
			} else if(in_array(Auth::user()->type,['STUDENT','NURSE']) && Auth::user()->verified == '1') {
				Auth::logout();
			}
			
            // Authentication passed...
            return redirect()->intended('/admin/dashboard');
        }
    }
	
    public function showRegistrationForm()
    {
        $roleCount = Role::count();
		if($roleCount != 0) {
			$userCount = User::count();
			if($userCount == 0) {
				return view('auth.register');
			} else {
				return redirect('login');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }
    
    public function showLoginForm()
    {
		$roleCount = Role::count();
		if($roleCount != 0) {
			$userCount = User::count();
			if($userCount == 0) {
				return redirect('register');
			} else {
				return view('auth.login');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        // TODO: This is Not Standard. Need to find alternative
        Eloquent::unguard();
        
        $employee = Employee::create([
            'name' => $data['name'],
            'designation' => "Super Admin",
            'mobile' => "8888888888",
            'mobile2' => "",
            'email' => $data['email'],
            'gender' => 'Male',
            'dept' => "1",
            'city' => "Pune",
            'address' => "Karve nagar, Pune 411030",
            'about' => "About user / biography",
            'date_birth' => date("Y-m-d"),
            'date_hire' => date("Y-m-d"),
            'date_left' => date("Y-m-d"),
            'salary_cur' => 0,
        ]);
        
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'context_id' => $employee->id,
            'type' => "Employee",
        ]);
        $role = Role::where('name', 'SUPER_ADMIN')->first();
        $user->attachRole($role);
    
        return $user;
    }
}
