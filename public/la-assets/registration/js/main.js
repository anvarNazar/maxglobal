


$(document).ready(function(){
    $('select').formSelect();

    $('.datepicker').datepicker({
        selectMonths: true,// Creates a dropdown to control month
        selectYears: 30 // Creates a dropdown of 15 years to control year,
    });
  });

  $('[name="uniselect"]').change(function () {
    if ($(this).val() === "5") {
      $('#uni-intsd').show();
  } else {
      $('#uni-intsd').hide();
  }
  });
  
  $('[name="courseselect"]').change(function () {
    if ($(this).val() === "5") {
      $('#course-intsd').show();
  } else {
      $('#course-intsd').hide();
  }
  });

  $('[name="invoice"]').change(function () {
    if ($(this).val() === "1") {
      $('#invoiced').show();
  } else {
      $('#invoiced').hide();
  }
  });

  $('[name="enrollment"]').change(function () {
    if ($(this).val() === "1") {
      $('#enrolled').show();
  } else {
      $('#enrolled').hide();
  }
  });

  $('[name="visastatus"]').change(function () {
    if ($(this).val() === "1") {
      $('#visa-rcd').show();
  } else {
      $('#visa-rcd').hide();
  }
  });

  $('[name="tuitionfeecheck"]').change(function () {
  if ($(this).val() === "1") {
    $('#tf-paid').show();
} else {
    $('#tf-paid').hide();
}
});

$('[name="cas"]').change(function () {
    if ($(this).val() === "1") {
      $('#cas-rcd').show();
  } else {
      $('#cas-rcd').hide();
  }
  });

$('[name="ofl"]').change(function () {

    if ($(this).val() === "1") {
  
      $('#ofl-upload').show();
  } else {
      $('#ofl-upload').hide();
  }
  });


  $('[name="degreedone"]').change(function () {
    if ($('[name="degreedone"]:checked').val() === "1") {
        $('#degree-details').show();
    } else {
        $('#degree-details').hide();
    }
});

  $('[name="ukdeg"]').change(function () {
    if ($('[name="ukdeg"]:checked').val() === "1") {
        $('#ukdegdone').show();
    } else {
        $('#ukdegdone').hide();
    }
});

$('[name="exp"]').change(function () {
    if ($('[name="exp"]:checked').val() === "1") {
        $('#havexp').show();
    } else {
        $('#havexp').hide();
    }
});

$('[name="studyhistory"]').change(function () {
    if ($('[name="studyhistory"]:checked').val() === "1") {
        $('#havsh').show();
    } else {
        $('#havsh').hide();
    }
});

$('[name="pgdone"]').change(function () {
    if ($('[name="pgdone"]:checked').val() === "1") {
        $('#degree-details').show();
    } else {
        $('#degree-details').hide();
    }
});
$('[name="ieltsdone"]').change(function () {
    if ($('[name="ieltsdone"]:checked').val() === "1") {
        $('#degree-details').show();
    } else {
        $('#degree-details').hide();
    }
});

$('[name="studiedabroad"]').change(function () {
    if ($('[name="studiedabroad"]:checked').val() === "1") {
        $('#studiedabroad').show();
    } else {
        $('#studiedabroad').hide();
    }
});

$('[name="visarefusal"]').change(function () {
    if ($('[name="visarefusal"]:checked').val() === "1") {
        $('#visarefusal').show();
    } else {
        $('#visarefusal').hide();
    }
});

$('[name="toeic"]').change(function () {
    if ($('[name="toeic"]:checked').val() === "1") {
        $('#toeic').show();
    } else {
        $('#toeic').hide();
    }
});

$('[name="criminalrecord"]').change(function () {
    if ($('[name="criminalrecord"]:checked').val() === "1") {
        $('#criminalrecord').show();
    } else {
        $('#criminalrecord').hide();
    }
});

$('[name="disabilities"]').change(function () {
    if ($('[name="disabilities"]:checked').val() === "1") {
        $('#disabilities').show();
    } else {
        $('#disabilities').hide();
    }
});